import pathlib
import setuptools
import distutils
import subprocess

from distutils.command.build import build as distutils_build

class BuildPyQt(setuptools.Command):
    description = "Compile PyQt UI files and resource files"
    user_options = [
        ('pyuic=',      None, 'pyuic executable'),
        ('pyrcc=',      None, 'pyrcc executable'),
        ('pyuic-opts=', None, 'pyuic extra options'),
        ('packages=',   None, 'List of comma separated packages in which to recursively find .qrc and .ui files'),
    ]

    def initialize_options(self):
        self.packages   = []
        self.pyuic      = 'pyuic5'
        self.pyrcc      = 'pyrcc5'
        self.pyuic_opts = ['--from-imports']

    def finalize_options(self):
        if isinstance(self.packages, str):
            self.packages = [_.strip() for _ in self.packages.split(',')]

    def run(self):
        for package in self.packages:
            path = pathlib.Path(package)

            if self.pyuic:
                uifiles = path.glob("**/*.ui")
                for uifile in uifiles:
                    compiled = f"{str(uifile).replace('.ui','_UI.py')}"
                    command = [self.pyuic, str(uifile), *self.pyuic_opts, '-o', compiled]
                    self.announce(' '.join(command), level=distutils.log.INFO)
                    ret = subprocess.call(command)
                    if ret != 0:
                        self.announce(f'error compiling .ui file: {uifile}', level=distutils.log.ERROR)

            if self.pyrcc:
                rccfiles = path.glob("**/*.qrc")
                #if len(rccfiles) > 0: self.announce(f"Compiling package '{package}' PyQt UI files...", level=distutils.log.INFO)
                for rccfile in rccfiles:
                    compiled = f"{str(rccfile).replace('.qrc','_rc.py')}"
                    command = [self.pyrcc, str(rccfile), '-o', compiled]
                    self.announce(' '.join(command), level=distutils.log.INFO)
                    ret = subprocess.call(command)
                    if ret != 0:
                        self.announce(f'error compiling .rcc file: {rccfile}', level=distutils.log.ERROR)


class build(distutils_build):
    sub_commands = [('build_qt', None)] + distutils_build.sub_commands


import logging

from PyQt5        import QtCore
from PyQt5        import QtGui
from PyQt5        import QtWidgets
from PyQt5.QtCore import Qt

from .Dockable          import Dockable
from .PlaylistDetail_UI import Ui_PlaylistDetail
from .QPlaylistModel    import QPlaylistModel
from mnectar.config     import Setting

from mnectar.config       import Configurable

_logger = logging.getLogger(__name__)


class PlaylistDetail(QtWidgets.QWidget):
    model_index_played = None
    mrl_playing = None

    search_string  = None
    default_search = Setting(default="")
    search_delay   = Setting(default=50)

    def __init__(self, *arg, **kw):
        super().__init__(*arg, **kw)
        self.config_ui()

    def config_ui(self):
        self.ui = Ui_PlaylistDetail()
        self.ui.setupUi(self)
        self.model = QPlaylistModel(self.window().app)

        self.ui.playlist_view.setName('Library')
        self.ui.playlist_view.setModel(self.model)

        self.ui.playlist_view.sortByColumn(0, Qt.AscendingOrder)

        self.ui.search.textEdited.connect(self.on_filter)

        self.ui.search.setText(self.default_search)
        self.model.playlist.active = True

        self.filter_timer = QtCore.QTimer()
        self.filter_timer.setSingleShot(True)
        self.filter_timer.timeout.connect(self.on_filter_delay)

        self.on_filter(self.default_search)

        # FIXME Once a changeable central widget exists, this should be made more generic
        # FIXME Possibly change this to playing / resuming the previous track played
        self.window().app.signal.playAfter.emit(self.model.playlist.pointer(0))

    def on_filter(self, searchstr):
        self.search_string  = searchstr

        # Delay filtering the playlist
        # ... because re-filtering on every character typed is needlessly time consuming
        # ... a small delay won't be noticed and improves perormance significantly
        if not self.filter_timer.isActive():
            self.filter_timer.start(self.search_delay)

    def on_filter_delay(self):
        self.model.filter(self.search_string)
        self.default_search = self.search_string


import collections
import functools
import lark
import logging
import operator
import tinydb

from .abc     import LibraryContent

_logger = logging.getLogger(__name__)

class LibraryRecordTinyDB(collections.ChainMap):
    def __init__(self, record, parent):
        self.parent = parent
        if isinstance(record, tinydb.database.Document):
            self.__doc_id = record.doc_id
        elif type(record) == int:
            self.__doc_id = record
        elif type(record) == str:
            self.__doc_id = self.parent.bymrl[record].doc_id

        self.__mrl    = self.parent.idMap[self.doc_id]['mrl']
        self.private  = {}

        super().__init__(self.private,
                         self.record['tags'],
                         self.record['meta'],
                         self.record.setdefault('plugin', dict()),
                         {'mrl': self.mrl})

    @property
    def record(self):
        return self.parent.idMap[self.__doc_id]

    @property
    def doc_id(self):
        return self.__doc_id

    @property
    def mrl(self):
        return self.__mrl

    def write(self, flush=False):
        self.parent.update(self)
        if flush: self.parent.flush()

    def __copy__(self):
        return type(self)(self.__mrl, self.parent)

    def __deepcopy__(self, memo):
        copy = type(self)(self.__mrl, self.parent)
        memo[id(self)] = copy
        return copy

    def __setitem__(self, item, value):
        if type(item) == str and item.startswith('_'):
            self.private.__setitem__(item, value)
        else:
            self.record['tags'].__setitem__(item, value)

    def __delitem__(self, item):
        if type(item) == str and item.startswith('_'):
            del self.private[item]
        else:
            del self.record[item]

    def __eq__(self, other):
        if type(other) == type(self) and other.doc_id == self.doc_id:
            return True
        elif type(other) == str and other == self.mrl:
            return True
        else:
            return False

    def __hash__(self):
        return hash((self.__mrl,self.__doc_id))

class LibraryContentTinyDB(LibraryContent):
    filename = None # Library Content Filename
    idMap    = None # Library content by tinydb doc_id
    mrlMap   = None # Library content by MRL in each document

    def __init__(self, filename=None):
        if filename is not None:
            self.read(filename)

    @property
    def raw(self):
        """The raw database content"""
        return self.idMap.values()

    @property
    def records(self):
        """Database records wrapped in a utility class"""
        return [LibraryRecordTinyDB(_, self) for _ in self.raw]

    @property
    def ids(self):
        """List of TinyDB record ids"""
        return list(self.idMap.keys())

    @property
    def mrls(self):
        """List of MRL strings accessible contained by this class"""
        return list(self.mrlMap.keys())

    @property
    def tags(self):
        """List of all available tags in the library"""
        return set(_ for record in self.records for _ in record.keys())

    @property
    def query(self):
        """Simplified access to the TinyDB 'Query' functionality"""
        return tinydb.Query()

    @property
    def where(self):
        """Simplified access to the TinyDB 'where' functionality"""
        return tinydb.where

    def read(self, filename):
        """Read database file content"""
        self.filename  = filename
        storage        = tinydb.storages.JSONStorage
        cache          = tinydb.middlewares.CachingMiddleware(storage)
        self.db        = tinydb.TinyDB(self.filename, storage=cache, indent=1)
        self.dbRecords = self.db.table('tracks')
        self.dbConfig  = self.db.table('config')
        self.idMap     = {_.doc_id: _ for _ in self.dbRecords.all()}
        self.mrlMap    = {_['mrl']: _ for _ in self.idMap.values()}

    def update(self, data, cond=None, *, table='tracks'):
        """Insert (if missing) or Update (if found) a record into the database"""
        if isinstance(data, LibraryRecordTinyDB):
            doc = data.record
            if cond is None:
                cond = tinydb.Query().mrl == data.mrl
        elif isinstance(data, collections.abc.Mapping):
            doc = data
        else:
            raise TypeError(f"Invalid Document Data Type: {type(data)}")

        self.db.table(table).upsert(doc, cond)

    def remove(self, cond, *, table='tracks'):
        self.db.table(table).remove(cond)

    def flush(self):
        self.db.storage.flush()

    def configSave(self, doc, cls):
        # Ensure 'the class name is set in the document
        doc.setdefault('class', cls.__class__.__name__)
        dbConfig = self.db.table('config')
        dbConfig.upsert(doc, tinydb.where('class') == doc['class'])
        self.db.storage.flush()

    def configLoad(self, cls):
        config = self.db.table('config').search(tinydb.where('class') == cls.__class__.__name__)
        if len(config) == 1:
            return config[0]
        elif len(config) > 1:
            _logger.error(f"Multiple '{self.__class__.__name__}' configurations: {config}")
        else:
            return {}

    def __contains__(self, item):
        if isinstance(item, LibraryRecordTinyDB):
            return item.doc_id in self.idMap
        elif type(item) == str:
            return item in self.mrlMap
        elif type(item) == int:
            return item in self.idMap
        elif isinstance(item, collections.abc.Mapping):
            return item in self.raw

    def __iter__(self):
        for recid in self.ids:
            yield LibraryRecordTinyDB(recid, self)

    def __getitem__(self, item):
        if isinstance(item, LibraryRecordTinyDB):
            return  LibraryRecordTinyDB(self.idMap[item.doc_id], self)
        elif type(item) == str:
            return LibraryRecordTinyDB(self.mrlMap[item], self)
        elif type(item) == int:
            return LibraryRecordTinyDB(self.idMap[item], self)
        else:
            raise KeyError(f"Library Content Not Found for Key: {item}")

    def __len__(self):
        return len(self.idMap)

    def __repr__(self):
        return f"{self.__class__.__name__}('{self.filename}')"

    def filter(self, query, content=None):
        """Filter the records using a test query function.

        The 'query' function must accept a Mapping object (which will be a database record)
        and return 'True' if the Mapping object should be included in the filter.

        TinyDB provides a powerful query method builder accessible via the 'where' and 'query'
        properties of this class, though any method may be used.
        """
        return [LibraryRecordTinyDB(_, self) for _ in filter(query, self.raw)]


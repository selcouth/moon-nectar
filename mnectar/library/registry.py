from ..config import Configurable
from .column  import Column
from .column  import ColumnManager
from .manager import LibraryManager

import Registry


class Library(Registry, Configurable, parent=Registry):
    @classmethod
    def create_config(cls, parser):
        parser.add_argument('--scan',
                            action  = 'append',
                            default = [],
                            help    = 'Scan the specified library directories and exit')

    @classmethod
    def create(cls, app):
        app.library = LibraryManager(app)

        Registry.Library.Columns.create(app)
        Registry.Library.SearchEngine.create(app)
        Registry.Library.Extensions.create(app)

        if len(app.config.scan) > 0:
            for dirname in app.config.scan:
                dirname = pathlib.Path(dirname).expanduser().absolute()
                app.library.scanDirectory(dirname)
            sys.exit(0)


class SearchEngine(Registry, parent=Library):
    @classmethod
    def create(cls, app):
        # XXX make this user selectable!
        app.search = cls.LogicSearchEngine(app)
        app.search.enable()


class Extensions(Registry, parent=Library):
    @classmethod
    def create(cls, app):
        if not app.config.plugins_disabled:
            if not hasattr(app.columns, '_extensions'):
                app.library._extensions = []

            for plugin in cls.plugins:
                app.library._extensions.append(plugin(app))
                app.library._extensions[-1].enable()


class Columns(Registry, parent=Registry.Library):
    @classmethod
    def create(cls, app):
        app.columns = ColumnManager()

        if not hasattr(app.columns, '_column_defs'):
            app.columns._column_defs = []

        for plugin in cls.plugins:
            app.columns._column_defs.append(plugin(app))
            app.columns._column_defs[-1].enable()


class BaseColumns(Registry.Plugin, registry=Registry.Library.Columns):
    standard_columns                                           = [
            Column('mrl',         'Filename', 400, hidden      = True),
            Column('discnumber',  'Disc',      50, displayFunc = lambda _: str(_),
                                                   sortFunc    = lambda _: f"{_:03d}",
                                                   sortCols    = ['album','discnumber','tracknumber'],
                                                   sortDefault = 0,
                                                   hidden      = True),
            Column('tracknumber', 'Track',     50, displayFunc = lambda _: str(_),
                                                   sortFunc    = lambda _: f"{_:03d}",
                                                   sortCols    = ['album','discnumber','tracknumber'],
                                                   sortDefault = 0),
            Column('title',       'Title',    200, filterAuto  = True),
            Column('artist',      'Artist',   200, filterAuto  = True,
                                                   sortCols    = ['artist', 'album','tracknumber']),
            Column('album',       'Album',    200, filterAuto  = True,
                                                   sortCols    = ['album','discnumber','tracknumber']),
            Column('genre',       'Genre',    200, filterAuto  = True,
                                                   displayFunc = lambda _: ', '.join(sorted(_)),
                                                   sortFunc    = lambda _: ','.join(sorted(_)).lower(),
                                                   sortDefault = []),
            Column('year',        'Year',      50, hidden      = True,
                                                   sortFunc    = lambda _: f"{_:04d}",
                                                   sortDefault = 0 ),
            Column('length',      'Length',    55, hidden      = True,
                                                   sortFunc    = lambda _: f"{int(_//60):05d}:{int(_%60):02d}",
                                                   displayFunc = lambda _: f"{int(_//60):d}:{int(_%60):02d}"),
            ]

    def enable(self):
        for column in self.standard_columns:
            self.app.columns.add(column)



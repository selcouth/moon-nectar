import Registry.Library.Columns

from mnectar.library.column import Column

class HashtagColumn(Registry.Plugin, registry=Registry.Library.Columns):
    def enable(self):
        self.app.columns.add(
            Column(
                "hashtag",
                "Hashtags",
                200,
                displayFunc = lambda _: ", ".join(sorted(_)),
                sortFunc    = lambda _: ",".join(sorted(_)).lower(),
                sortDefault = [],
                hidden      = True,
            )
        )

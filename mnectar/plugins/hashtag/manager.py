import logging

from contextlib                       import contextmanager
from PyQt5.QtCore                     import Qt
from PyQt5                            import QtCore
from PyQt5                            import QtWidgets
from mnectar.config                   import Setting
from mnectar.registry                 import Registry, Plugin, PluginSetting
from mnectar.ui.pyqt5.Dockable        import Dockable
from mnectar.ui.pyqt5                 import Action
from .manager_UI                      import Ui_HashtagManager

import Registry.UI.PyQt

_logger = logging.getLogger(__name__)

class HashtagModel(QtCore.QAbstractTableModel):
    def __init__(self, *arg, records=None, **kw):
        super().__init__(*arg, **kw)
        self.app = self.parent().app

        self.setRecords(records)

    def setRecords(self, records=None):
        self.records = records
        self.refresh()

    @contextmanager
    def _layoutChangeManager(self):
        try:
            # Send a signal that the layout will be changing
            self.layoutAboutToBeChanged.emit([], self.VerticalSortHint)

            # Save the old persistent index list
            # ... so they can be updated later
            old_persist_list = self.persistentIndexList()

            yield
        finally:
            # Create the new persistent index objects

            if len(old_persist_list) > 0:
                invalid = [(_,QtCore.QModelIndex()) for _ in old_persist_list]
                self.changePersistentIndexList([_[0] for _ in invalid], [_[1] for _ in invalid])

            self.layoutChanged.emit([], self.VerticalSortHint)

    def refresh(self):
        with self._layoutChangeManager():
            if self.records is None:
                tags = set(tag
                           for rec in self.app.library.content
                           for tag in rec.get('hashtag', [])
                           )
            else:
                tags = set(tag
                           for rec in self.records
                           for tag in rec.get('hashtag',[])
                           )

            self.hashtags = sorted(tags)

    def rowCount(self, parent=None):
        return len(self.hashtags)

    def columnCount(self, parent=None):
        return 1

    def data(self, index, role, trackStrSort=False):
        if role == QtCore.Qt.DisplayRole:
            return self.hashtags[index.row()]

class HashtagManager(QtWidgets.QWidget, Plugin, Dockable,
                     registry  = Registry.UI.PyQt.Docked,
                     menu      = 'View',
                     menu_name = 'Hashtag Manager',
                     menu_key  = "Ctrl+T",
                     location  = Qt.RightDockWidgetArea):

    def __init__(self, *arg, **kw):
        super().__init__(*arg, **kw)

        self.records = []

        self.config_ui()

    def config_ui(self):
        # Create the UI
        self.ui = Ui_HashtagManager()
        self.ui.setupUi(self)

        # Create the models
        self.model_all     = HashtagModel(self, records=None)
        self.model_current = HashtagModel(self, records=[])

        # Configure the filter proxies
        self.proxy_all = QtCore.QSortFilterProxyModel(self)
        self.proxy_all.setSourceModel(self.model_all)
        self.proxy_all.setFilterCaseSensitivity(Qt.CaseInsensitive)
        self.proxy_all.setDynamicSortFilter(True)
        self.ui.all.setModel(self.proxy_all)

        self.proxy_current = QtCore.QSortFilterProxyModel(self)
        self.proxy_current.setSourceModel(self.model_current)
        self.proxy_current.setFilterCaseSensitivity(Qt.CaseInsensitive)
        self.proxy_current.setDynamicSortFilter(True)
        self.ui.current.setModel(self.proxy_current)

        # Setup signals
        self.ui.entry  .textChanged.connect(self.proxy_all.setFilterFixedString)
        self.ui.entry  .textChanged.connect(self.proxy_current.setFilterFixedString)
        self.ui.refresh.clicked    .connect(self.model_all.refresh)
        self.ui.add    .clicked    .connect(self.on_add)
        self.ui.remove .clicked    .connect(self.on_remove)
        self.ui.all    .activated  .connect(self.on_all_tags_activated)
        self.ui.current.activated  .connect(self.on_current_tags_activated)

        self.app.signal.playing.connect(self.on_playing)
        self.app.signal.pyqt.selected.connect(self.on_selection)

    def on_playing(self, pointer, length):
        if self.ui.playing.isChecked():
            self.records = [self.app.library.content[pointer.mrl]]
            self.update_current_tags()

            self.ui.selected.setText(
                "<style>p {margin: 0; padding:0;}</style>"
                + f"""<p><b>{pointer.record['title']}</b></p>"""
            )

    def on_selection(self, mrls):
        if self.ui.selection.isChecked():
            content = self.app.library.content
            self.records = [content[_] for _ in mrls]
            self.update_current_tags()

            if len(self.records) > 0:
                self.ui.selected.setText(
                    "<html>"
                    + "<style>p {margin: 0; padding:0;}</style>"
                    + f"""<p><b>{self.records[0]['title']}</b></p>"""
                    + (
                        f"""<p align="right" style="font-size:10pt;"><i>+{len(self.records)-1} more</i></p>"""
                        if len(self.records) > 1
                        else ""
                    )
                    + "</html>"
                )
            else:
                self.ui.selected.setText(
                    """
                    <p></p>
                    """
                )

    def update_current_tags(self):
        self.model_current.setRecords(self.records)

    def on_add(self, *, tag=None):
        if tag is None:
            tag = self.ui.entry.text()

        modified = False

        for record in self.records:
            if tag not in record.record['plugin'].setdefault('hashtag', list()):
                record.record['plugin']['hashtag'].append(tag)
                record.write(False)
                modified = True

        if modified:
            self.model_current.refresh()
            self.model_all.refresh()
            self.app.library.content.flush()

    def on_remove(self, *, tag=None):
        if tag is None:
            tag = self.ui.entry.text()

        modified = False

        for record in self.records:
            if tag in record.record['plugin'].get('hashtag', list()):
                record.record['plugin']['hashtag'].remove(tag)
                record.write(False)
                modified = True

        if modified:
            self.model_current.refresh()
            self.model_all.refresh()
            self.app.library.content.flush()

    def on_all_tags_activated(self, index):
        if index.isValid():
            tag = self.ui.all.model().data(index, Qt.DisplayRole)
            self.on_add(tag=tag)

    def on_current_tags_activated(self, index):
        if index.isValid():
            tag = self.ui.current.model().data(index, Qt.DisplayRole)
            self.on_remove(tag=tag)

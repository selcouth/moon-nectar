import logging
import pytest

from mnectar.appinit                   import AppInit
from mnectar.library.LarkSearchEngine  import LarkSearchEngine
from mnectar.library.column            import ColumnManager
from mnectar.registry                  import Registry

data = {1:   {'album': "'d' flute album",
              'genre': ['Irish Traditional'],
              'copyright': '2006 Green Linnet',
              'year': 2006,
              'title': "George White's Favourite & The Silgo Maid",
              'artist': 'Kevin Crawford',
              'tracknumber': 1,
              'length': 208.1},
        2:   {'album': "'d' flute album",
              'genre': ['Irish Traditional'],
              'copyright': '2006 Green Linnet',
              'year': 2006,
              'title': "Jim Donoghue's, Gorman's & Kiss Me Kate",
              'artist': 'Kevin Crawford',
              'tracknumber': 2,
              'length': 205.2},
        3:   {'album': "'d' flute album",
              'genre': ['Irish Traditional'],
              'copyright': '2006 Green Linnet',
              'year': 2006,
              'title': "Christy Barry's Set",
              'artist': 'Kevin Crawford',
              'tracknumber': 3,
              'length':252.3},
        4:   {'album': 'Matt Molloy, Paul Brady, Tommy Peoples',
              'genre': ['Irish Traditional'],
              'year': 1978,
              'encodedby': 'iTunes v4.8.0.31',
              'grouping': 'celtic',
              'title': 'Matt Peoples (Reels)',
              'artist': 'Matt Molloy, Paul Brady, Tommy Peoples',
              'discnumber': 1,
              'tracknumber': 1,
              'length': 99.4},
        5:   {'album': 'Set Dances of Ireland Vol.1',
              'genre': [' Irish Traditional'],
              'title': 'The Boys of Ballisodare/Rolling in the Ryegrass [Reels]',
              'artist': 'Junior Crehan, Eamon McGivney, Michael Tubridy & Tommy McCarthy',
              'tracknumber': 4,
              'length': 99.9},
        6:   {'album': 'The High Part Of The Road',
              'genre': ['Irish Traditional'],
              'year': 2005,
              'encodedby': 'iTunes 8.0.2.20',
              'grouping': 'celtic',
              'title': 'The Nine Points Of Roguery, Bean An It Ar Lar (The Mistress On The Floor)',
              'artist': 'Tommy Peoples',
              'discnumber': 1,
              'tracknumber': 6,
              'length': 192.3},
        7:   {'album': 'The Legend (disc 4: Family and Friends)',
              'genre': ['Country', 'Folk', 'Rock'],
              'year': 2005,
              'releasedate': '2005-08-29',
              'title': 'Silver Haired Daddy of Mine (feat. Tommy Cash)',
              'artist': 'Johnny Cash',
              'tracknumber': 15,
              'length': 168},
        8:   {'album': 'Upside Down',
              'genre': ['Celtic Folk'],
              'year': 2006,
              'title': "Tommy Peoples' Grainne's Jig; Kilfenora Jig",
              'artist': 'Sharon Shannon, Michael McGoldrick, Jim Murray & Dezi Donnelly',
              'tracknumber': 2,
              'length': 900,
              'hashtag': ['irish','fiddle']},
        }

searches = {
    # Test an empty string returns everything
    "":                                     list(data.values()),

    # Test Quoting oddities
    '''album:"'d' flute"''':                 [data[_] for _ in [1,2,3]],
    '''album="'d' flute album"''':           [data[_] for _ in [1,2,3]],
    '''album="\\'d\\' flute album"''':       [data[_] for _ in [1,2,3]],
    """album='\\'d\\' flute album'""":       [data[_] for _ in [1,2,3]],

    # Test auto-column search
    '''Tommy''':                             [data[_] for _ in [4,5,6,7,8]],
    '''tommy''':                             [data[_] for _ in [4,5,6,7,8]],
    '''Tommy Peoples''':                     [data[_] for _ in [4,6,8]],
    '''peoples''':                           [data[_] for _ in [4,6,8]],

    # Test auto-column search with regex
    '''/Tommy''':                            [data[_] for _ in [4,5,6,7,8]],
    '''/Peoples''':                          [data[_] for _ in [4,6,8]],
    '''/Tom.*Peoples''':                     [data[_] for _ in [4,6,8]],

    # Test column search: startswith
    '''genre:Celtic F''':                    [data[_] for _ in [8]],
    '''genre:celtic f''':                    [data[_] for _ in [8]],
    '''genre:Celtic F/I''':                  [data[_] for _ in [8]],
    '''genre:celtic f/I''':                  [data[_] for _ in []],
    '''genre:celtic f''':                    [data[_] for _ in [8]],
    '''genre:celtic f/i''':                  [data[_] for _ in [8]],
    '''year:''':                             list(_ for _ in data.values() if 'year' in _),

    # Test column search: exact match
    '''genre=Country''':                     [data[_] for _ in [7]],
    '''genre=country''':                     [data[_] for _ in [7]],
    '''genre=country/i''':                   [data[_] for _ in [7]],
    '''genre=country/I''':                   [data[_] for _ in []],
    '''genre=Country/I''':                   [data[_] for _ in [7]],
    '''year=''':                             list(_ for _ in data.values() if 'year' not in _),

    # Test column search boolean logic
    '''genre:(Celtic F)''':                  [data[_] for _ in [8]],
    '''genre:("Celtic F")''':                [data[_] for _ in [8]],
    '''genre=(Country | Celtic Folk)''':     [data[_] for _ in [7,8]],
    '''genre=("Country" | "Celtic Folk")''': [data[_] for _ in [7,8]],
    '''genre=(Country & Rock)''':            [data[_] for _ in [7]],
    '''genre:(Country & Rock | Celtic F)''': [data[_] for _ in [7,8]],
    '''year=!2006''':                        list(_ for _ in data.values() if _.get('year',None) != 2006),
    '''year:!200''':                         list(_ for _ in data.values() if not str(_.get('year',0)).startswith('200')),

    # Test column search: regex search
    """album/Tommy Peoples""":               [data[_] for _ in [4]],
    """album/Tommy Peoples/""":              [data[_] for _ in [4]],
    """album/Tommy Peoples/I""":             [data[_] for _ in [4]],
    """album/tommy peoples/i""":             [data[_] for _ in [4]],
    """album/tommy peoples/I""":             [data[_] for _ in []],
    '''genre/(Count|Celt.*lk)/''':           [data[_] for _ in [7,8]],
    '''encodedby/''':                        list(_ for _ in data.values() if 'encodedby' in _),

    # Test multi-search logic
    ''' (year=2006) & ((title:Christy) | (tracknumber=1))''': [data[_] for _ in [1,3]],
    ''' (year=2006) &  (title:Christy) | (tracknumber=1) ''': [data[_] for _ in [1,3,4]],
    '''((year=2006) & (title:Christy)) | (tracknumber=1) ''': [data[_] for _ in [1,3,4]],
    '''(genre=Country) | (year=1978)''':                      [data[_] for _ in [4,7]],
    '''(genre:Irish) & (tracknumber=1)''':                    [data[_] for _ in [1,4]],
    '''year>=1970&year<=1979''':                              list(_ for _ in data.values() if _.get('year',0)//10*10 == 1970),
    '''!(year>=1970&year<=1979)''':                           list(_ for _ in data.values() if _.get('year',0)//10*10 != 1970),

    # Numeric Comparison Tests
    '''year:197''':                          [data[_] for _ in [4]],
    '''year:1978''':                         [data[_] for _ in [4]],
    '''year:978''':                          [data[_] for _ in []],
    '''year=2006''':                         [data[_] for _ in [1,2,3,8]],
    '''year=1978|2006''':                    [data[_] for _ in [1,2,3,4,8]],
    '''length=15:0''':                       [data[_] for _ in [8]],
    '''length=0:15:0''':                     [data[_] for _ in [8]],
    '''length<99.9''':                       [data[_] for _ in [4]],
    '''length<=99.9''':                      [data[_] for _ in [4,5]],
    '''year>2005''':                         [data[_] for _ in [1,2,3,8]],
    '''year>=2005''':                        [data[_] for _ in [1,2,3,6,7,8]],

    # Numeric Comparison Tests: Ensure string matching is not doing something strange
    '''year>2''':                            list(_ for _ in data.values() if 'year' in _),
    '''year>20''':                           list(_ for _ in data.values() if 'year' in _),
    '''year>=200''':                         list(_ for _ in data.values() if 'year' in _),
    '''year<20190''':                        list(_ for _ in data.values() if 'year' in _),
    '''year<=20190''':                       list(_ for _ in data.values() if 'year' in _),

    # Hashtag Tests
    '''#irish''':                            list(_ for _ in data.values() if 'irish' in _.get('hashtag',[])),
    '''#''':                                 list(_ for _ in data.values() if 'hashtag' in _),
}

invalid = [
    '''genre=(''',
    '''genre="''',
    """genre='""",
    """genre=('foo)""",
    """genre=('foo")""",
    """genre=("foo')""",
    """genre=(foo')""",
    """genre=(foo")""",
    """foo=bar""",
    '''/To[m.*Peoples''',
]

grammar_invalid = [
    {'Foo': 'bar'},
    [],
    {1:'foo'},
    {'%foo': [("1","2","3")]},
    {'foo': [("1","2","3")]},
    {'FOO': [("1","2","3")]},
    {'foo': 123},
]

grammar_valid = [
    ({'%import': ['common.WS']}, '%import common.WS'),
    ({'%import': [('common.WS', 'white_space')]}, '%import common.WS -> white_space'),
]

def setup_module(module):
    module.logger = logging.getLogger('mnectar.library.LarkSearchEngine')
    module.logger_old_level = logger.level
    module.logger.setLevel('DEBUG3')

def teardown_module(module):
    module.logger.setLevel(module.logger_old_level)

class TestSearchEngine:

    @classmethod
    def setup_class(cls):
        Registry.Library.Columns.create(cls)
        cls.engine = Registry.Library.SearchEngine.LogicSearchEngine(cls)
        cls.engine.enable()

    def test_search_magic(self):
        search_string = 'Test Search'
        library_data  = [{_.name:search_string} for _ in self.columns.list()]
        auto          = [_.name for _ in self.columns.list() if _.filterAuto]
        expected      = [{_.name:search_string} for _ in self.columns.list() if _.filterAuto]
        filtered      = self.engine.filter(library_data, search_string)

        assert len(filtered) == len(auto)
        assert filtered == expected

    @pytest.mark.parametrize("search,expected", searches.items())
    def test_search(self, search, expected):
        print(self.engine._grammar.grammar)
        filtered = self.engine.filter(data.values(), search)
        assert expected == filtered

    @pytest.mark.parametrize("search,expected", searches.items())
    def test_search_modifier(self, search, expected):
        print(self.engine._grammar.grammar)
        to_search = enumerate(data.values())
        filtered = self.engine.filter(to_search, search, lambda _: _[1])
        assert expected == [_[1] for _ in filtered]

    @pytest.mark.parametrize("search", invalid)
    def test_search_exception(self, search):
        filtered = self.engine.filter(data.values(), search)
        assert [] == filtered


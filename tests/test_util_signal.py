import copy
import logging
import pytest

from mnectar.util.signal import Signal
from mnectar.util.signal import BoundSignal
from mnectar.util.signal import call_via_signal


def setup_module(module):
    module.logger = logging.getLogger("mnectar.util.signal")
    module.logger_old_level = logger.level
    module.logger.setLevel("DEBUG3")


def teardown_module(module):
    module.logger.setLevel(module.logger_old_level)


@pytest.fixture
def SigTest(mocker):
    class SignalTester:
        sig = Signal()

    return SignalTester


def test_signal_autoconnect():
    @Signal.autoconnect
    class cb_class:
        sig = Signal()

        @sig.connect
        def cb_method():
            pass

    cb_obj = cb_class()
    slot = cb_obj.cb_method
    is_signal = False

    assert hasattr(cb_obj.cb_method, "_signals")
    assert cb_obj.sig.connected(slot)
    assert cb_obj.sig.disconnect(slot)
    assert not cb_obj.sig.connected(slot)
    assert not cb_obj.sig.disconnect(slot)


def test_signal_disp_qtsig_to_method(qtbot, mocker):
    from PyQt5 import QtCore

    class SignalTester:
        sig1 = Signal("qt-app")

        def cb_method_1(self):
            pass

    sigtest = SignalTester()
    mocker.spy(sigtest, "cb_method_1")
    sigtest.sig1.connect(sigtest.cb_method_1)
    sigtest.sig1._dispatchers[0].setup()

    with qtbot.waitSignal(sigtest.sig1._dispatchers[0].qt_sig_runner.sig_run):
        sigtest.sig1.emit()

    sigtest.cb_method_1.assert_called_once()


def test_signal_disp_qtsig_to_method_fail_no_qt_app(mocker):
    from PyQt5 import QtCore

    # Mimic no application created .....
    mocker.patch("PyQt5.QtCore.QCoreApplication.instance", return_value=None)

    assert QtCore.QCoreApplication.instance() is None

    class SignalTester:
        sig1 = Signal("qt-app")

        def cb_method_1(self):
            pass

    sigtest = SignalTester()
    mocker.spy(sigtest, "cb_method_1")
    sigtest.sig1.connect(sigtest.cb_method_1)

    sigtest.sig1.emit()

    sigtest.cb_method_1.assert_not_called()


def test_signal_disp_qtsig_to_sig(qtbot, mocker):
    from PyQt5 import QtCore

    class SignalTester:
        sig1 = Signal("qt-app")
        sig2 = Signal("qt-app")

        def cb_method_1(self):
            pass

        def cb_method_2(self):
            pass

    sigtest = SignalTester()
    mocker.spy(sigtest, "cb_method_1")
    mocker.spy(sigtest, "cb_method_2")
    sigtest.sig1.connect(sigtest.sig2)
    sigtest.sig1.connect(sigtest.cb_method_1)
    sigtest.sig2.connect(sigtest.cb_method_2)
    sigtest.sig1._dispatchers[0].setup()
    sigtest.sig2._dispatchers[0].setup()

    with qtbot.waitSignal(sigtest.sig2._dispatchers[0].qt_sig_runner.sig_run):
        with qtbot.waitSignal(sigtest.sig1._dispatchers[0].qt_sig_runner.sig_run):
            sigtest.sig1.emit()

    sigtest.cb_method_1.assert_called_once()
    sigtest.cb_method_2.assert_called_once()


def test_signal_disp_qtsig_to_pyqt_sig(qtbot, mocker):
    from PyQt5 import QtCore

    class SignalTester(QtCore.QObject):
        sig1 = Signal("qt-app")
        sig2 = QtCore.pyqtSignal()

        def cb_method_1(self):
            pass

        def cb_method_2(self):
            pass

    sigtest = SignalTester()
    mocker.spy(sigtest, "cb_method_1")
    mocker.spy(sigtest, "cb_method_2")
    sigtest.sig1.connect(sigtest.sig2)
    sigtest.sig1.connect(sigtest.cb_method_1)
    sigtest.sig2.connect(sigtest.cb_method_2)
    sigtest.sig1._dispatchers[0].setup()

    with qtbot.waitSignal(sigtest.sig2):
        with qtbot.waitSignal(sigtest.sig1._dispatchers[0].qt_sig_runner.sig_run):
            sigtest.sig1.emit()

    sigtest.cb_method_1.assert_called_once()
    sigtest.cb_method_2.assert_called_once()


def test_signal_disp_direct_to_method(mocker):
    class SignalTester:
        sig1 = Signal("direct")

        def cb_method_1(self):
            pass

    sigtest = SignalTester()
    mocker.spy(sigtest, "cb_method_1")
    sigtest.sig1.connect(sigtest.cb_method_1)

    sigtest.sig1.emit()

    sigtest.cb_method_1.assert_called_once()


def test_signal_disp_direct_to_sig(mocker):
    class SignalTester:
        sig1 = Signal("direct")
        sig2 = Signal("direct")

        def cb_method_1(self):
            pass

        def cb_method_2(self):
            pass

    sigtest = SignalTester()
    mocker.spy(sigtest, "cb_method_1")
    mocker.spy(sigtest, "cb_method_2")
    sigtest.sig1.connect(sigtest.sig2)
    sigtest.sig1.connect(sigtest.cb_method_1)
    sigtest.sig2.connect(sigtest.cb_method_2)

    sigtest.sig1.emit()

    sigtest.cb_method_1.assert_called_once()
    sigtest.cb_method_2.assert_called_once()


def test_signal_disp_invalid(mocker):
    with pytest.raises(ValueError):

        class SignalTester:
            sig1 = Signal(["invalid", "direct"])

            def cb_method_1(self):
                pass

        sigtest = SignalTester()
        mocker.spy(sigtest, "cb_method_1")
        sigtest.sig1.connect(sigtest.cb_method_1)
        sigtest.sig1._dispatchers[0].setup()

        with qtbot.waitSignal(sigtest.sig1._dispatchers[0].qt_sig_runner.sig_run):
            sigtest.sig1.emit()

        sigtest.cb_method_1.assert_called_once()


def test_signal_invalid_slot(SigTest):
    def slot():
        pass

    sigtest = SigTest()

    # Test Error: Non-Connectable Type
    with pytest.raises(TypeError):
        con_key = sigtest.sig.connect(None)

    # Test Error: Disconnect Non-Connectable Type
    assert not sigtest.sig.disconnect(None)

    # Test Error: Invalid Types are never connected
    assert not sigtest.sig.connected(None)


@pytest.mark.parametrize(
    "setup",
    [
        "func",
        "method",
        "signal",
        "signal_emit",
        "pyqt_signal",
        "pyqt_signal_emit",
        "lambda",
    ],
)
def test_signal_connect(SigTest, setup):
    if setup == "func":

        def slot():
            pass

        is_signal = False

    elif setup == "method":

        class cb_class:
            def cb_method():
                pass

        cb_obj    = cb_class()
        slot      = cb_obj.cb_method
        is_signal = False

    elif setup == "signal":
        sigtest2  = SigTest()
        slot      = sigtest2.sig
        is_signal = True

    elif setup == "signal_emit":
        sigtest2  = SigTest()
        slot      = sigtest2.sig.emit
        is_signal = True

    elif setup == "pyqt_signal":
        QtCore = pytest.importorskip("PyQt5.QtCore")

        class cb_class(QtCore.QObject):
            pyqt_sig = QtCore.pyqtSignal()

        cb_obj    = cb_class()
        slot      = cb_obj.pyqt_sig
        is_signal = True

    elif setup == "pyqt_signal_emit":
        QtCore = pytest.importorskip("PyQt5.QtCore")

        class cb_class(QtCore.QObject):
            pyqt_sig = QtCore.pyqtSignal()

        cb_obj    = cb_class()
        slot      = cb_obj.pyqt_sig.emit
        is_signal = True

    elif setup == "lambda":
        slot = lambda: True
        is_signal = False

    sigtest = SigTest()
    con_key = sigtest.sig.connect(slot)

    assert con_key.signal == is_signal
    assert sigtest.sig.connected(slot)
    assert sigtest.sig.connected(con_key)
    assert sigtest.sig.disconnect(slot)
    assert not sigtest.sig.connected(slot)
    assert not sigtest.sig.connected(con_key)
    assert not sigtest.sig.disconnect(con_key)

    con_key_2 = sigtest.sig.connect(slot)

    assert con_key == con_key_2

    assert con_key_2.signal == is_signal
    assert sigtest.sig.connected(slot)
    assert sigtest.sig.connected(con_key_2)
    assert sigtest.sig.disconnect(con_key_2)
    assert not sigtest.sig.connected(con_key_2)
    assert not sigtest.sig.connected(con_key_2)
    assert not sigtest.sig.disconnect(con_key_2)

    # Verify behavior after original signal is deleted
    # ... This is unlikely but needs to be tested in case it comes up

    con_key_3 = sigtest.sig.connect(slot)
    sig       = sigtest.sig

    assert sig.connected(con_key_3)
    del sigtest
    assert "dead" in repr(sig)
    assert not sig.connected(con_key_3)
    assert sig.disconnect(con_key_3)
    assert sig.disconnect(con_key_3)
    assert sig.connect(slot) is None


def test_signal_autodelete_func():
    # Verify auto-disconnect of weakref slots
    class SignalTester:
        sig = Signal()

    def slot():
        pass

    sigtest = SignalTester()
    con_key = sigtest.sig.connect(slot)
    del slot
    import gc

    gc.collect()
    assert not sigtest.sig.connected(con_key)


def test_signal_class_access():
    class SignalTester:
        sig = Signal()

    assert isinstance(SignalTester.sig, Signal)


def test_signal_decorator(qtbot, mocker):
    class SignalTester:
        @call_via_signal
        def func(self):
            # NOTE: This value should not be accessible as a return value
            # ... because signal return values are always dropped
            return True

    # Create two instances of the test class to ensure that calling the method from one
    # instance does not cross over into another instance.
    sigtest1 = SignalTester()
    sigtest2 = SignalTester()

    mocker.spy(sigtest1, "func")
    mocker.spy(sigtest2, "func")

    assert sigtest1.func() is None
    sigtest1.func.assert_called_once()
    sigtest2.func.assert_not_called()

    assert sigtest2.func() is None
    sigtest1.func.assert_called_once()
    sigtest2.func.assert_called_once()

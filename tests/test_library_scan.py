import logging
import os
import pathlib
import pytest

from mnectar.library import LibraryManager
from mnectar.library import ColumnManager
from mnectar.config  import Configurable


def setup_module(module):
    module.logger = logging.getLogger('mnectar.library.Library')
    module.logger_old_level = logger.level
    module.logger.setLevel('DEBUG3')

    import Registry
    Registry.load_entry_points()


def teardown_module(module):
    module.logger.setLevel(module.logger_old_level)


@pytest.mark.incremental
class TestLibrary:
    @pytest.fixture(autouse=True)
    def setup(self, tmp_path):
        self.config       = Configurable.config_parse_args([])
        self.columns      = ColumnManager()
        self.library_dir  = pathlib.Path(__file__).with_name('data')
        self.library_file = tmp_path/'test.json'
        self.library      = LibraryManager( self, filename = self.library_file)

    def test_scan(self):
        self.library.scanDirectory(self.library_dir)
        libfiles = [os.path.join(path[0],filename) for path in os.walk(self.library_dir, followlinks=True)
                                                   for filename in path[2]
                                                   if not filename.startswith('.') and not filename.endswith('.json')]
        self.library.content.read(self.library_file)
        assert len(self.library.content) == len(libfiles)


import copy
import logging
import os
import pathlib
import platform
import pytest
import mnectar

import Registry.Backend.BackendMock

def setup_module(module):
    module.logger = logging.getLogger("mnectar.backend")
    module.logger_old_level = logger.level
    module.logger.setLevel("DEBUG3")


def teardown_module(module):
    module.logger.setLevel(module.logger_old_level)


def test_backend_mock_startup_failure(app):
    # Test Setup: Configure the backend
    backend = Registry.Backend.BackendMock(app)
    backend._running = True
    backend.enable()
    assert not backend.enabled

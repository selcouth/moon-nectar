import copy
import logging
import pytest

from mnectar.library.column import Column
from mnectar.library.column import ColumnManager

def setup_module(module):
    module.logger = logging.getLogger('mnectar.library.column')
    module.logger_old_level = logger.level
    module.logger.setLevel('DEBUG')

def teardown_module(module):
    module.logger.setLevel(module.logger_old_level)

def test_column_name():
    with pytest.raises(TypeError):
        column = Column()

def test_column_missing_description():
    with pytest.raises(TypeError):
        column = Column('foo')

def test_column_default():
    column = Column('foo','bar')

    assert column.name                 == 'foo'
    assert column.description          == 'bar'
    assert column.sizeHint             == -1
    assert column.display              == True
    assert column.hidden               == False
    assert column.filterAuto           == False
    assert column.displayFunc('FuBar') == 'FuBar'
    assert column.sortFunc('FuBar')    == 'fubar'
    assert column.sortCols             == [column.name]
    assert column.sortDefault          == ""


@pytest.fixture
def columns():
    columns    = [Column('col1', 'Column 1'),
                  Column('col2', 'Column 2', filterAuto = True),
                  Column('col3', 'Column 3', filterAuto = True),
                  Column('col4', 'Column 4', sortCols = ['col2', 'col3', 'col4'])]
    return columns

def test_column_manager_basic(columns):
    manager = ColumnManager()
    manager.add(columns[0])
    manager.extend(columns[1:])

    with pytest.raises(TypeError):
        manager.add(object)
    with pytest.raises(TypeError):
        manager['col1'] = object
    with pytest.raises(KeyError):
        manager['foo'] = columns[0]

    assert manager.list()         == columns
    assert manager.names()        == [_.name for _ in columns]
    assert manager.filterAsList() == columns
    assert manager.filterAsDict() == {_.name:_ for _ in columns}
    assert manager.index()        == [index for index,col in enumerate(columns)]
    assert manager                == {_.name:_ for _ in columns}

    assert [manager[_] for _ in range(len(columns))]       == [columns[_] for _ in range(len(columns))]
    assert [manager.indexOfName(_.name) for _ in columns] == [index for index,col in enumerate(columns)]

    manager.add(columns[2])
    assert manager.list()  == columns

    newcol = Column('new', 'New Column')
    manager[newcol.name] = newcol
    assert manager[-1] == newcol

def test_column_manager_filter(columns):
    manager = ColumnManager()
    manager.extend(columns)

    assert manager.list() == columns

    assert manager.names(filterAuto=True)        == [_.name for _ in columns if _.filterAuto]
    assert manager.filterAsList(filterAuto=True) == [_ for _ in columns if _.filterAuto]
    assert manager.filterAsDict(filterAuto=True) == {_.name:_ for _ in columns if _.filterAuto}

def test_column_manager_update(columns):
    manager    = ColumnManager()
    manager2   = ColumnManager()
    updateCols = [Column('col5', 'Column 5'),
                  Column('col6', 'Column 6')]
    combined   = columns+updateCols

    manager.extend(columns)
    manager2.extend(updateCols)

    manager.update(manager2)
    assert manager.list() == combined

def test_column_manager_copy(columns):
    manager  = ColumnManager()
    manager2 = ColumnManager(manager)
    manager3 = manager.copy()
    manager4 = copy.copy(manager)

    assert isinstance(manager3, ColumnManager)
    assert isinstance(manager4, ColumnManager)
    assert manager2 == manager
    assert manager3 == manager
    assert manager4 == manager

def test_column_manager_remove(columns):
    manager    = ColumnManager()
    manager.extend(columns)

    removed = manager.remove('col4')
    assert manager.list() == columns[:-1]
    assert removed.name == 'col4'

    removed = manager.remove('col1')
    assert manager.list() == columns[1:-1]
    assert removed.name == 'col1'




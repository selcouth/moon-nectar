import logging
import platform
import pytest

from mnectar.util import oscheck


def setup_module(module):
    module.logger = logging.getLogger("mnectar.util.decorator")
    module.logger_old_level = logger.level
    module.logger.setLevel("DEBUG3")


def teardown_module(module):
    module.logger.setLevel(module.logger_old_level)


def test_oscheck_valid_value():
    class Foo:
        @oscheck(target=platform.system(), return_value=False)
        def func(self):
            return True

    assert Foo().func()


def test_oscheck_valid_no_value():
    class Foo:
        @oscheck(target=platform.system())
        def func(self):
            return True

    assert Foo().func()


def test_oscheck_valid_factory():
    class Foo:
        @staticmethod
        def factory():
            return False

        @oscheck(target=platform.system(), return_factory=factory)
        def func(self, arg):
            return arg

    assert Foo().func(True)


def test_oscheck_invalid_value():
    class Foo:
        @oscheck(target="InvalidOS", return_value=False)
        def func(self):
            return True

    assert not Foo().func()


def test_oscheck_invalid_no_value():
    class Foo:
        @oscheck(target="InvalidOS")
        def func(self):
            return True

    assert Foo().func() is None


def test_oscheck_invalid_factory():
    class Foo:
        @oscheck(target="InvalidOS", return_factory=lambda: False)
        def func(self, arg):
            return arg

    class Bar:
        @oscheck(target="InvalidOS", return_factory=list)
        def func(self, arg):
            return [arg]

    assert not Foo().func(True)
    assert Bar().func(target="Hello") == []


def test_oscheck_wrong_args():
    with pytest.raises(ValueError):

        class Foo:
            @oscheck(target="InvalidOS", return_value=True, return_factory=list)
            def func(self):
                return True

        Foo().func()

    with pytest.raises(ValueError):

        class Foo:
            @oscheck(return_value=True)
            def func2(self):
                return False

import pytest
import random

from dataclasses import FrozenInstanceError

from mnectar.library.view import Sorted, Filtered, View, Editable, Changed, ViewPointer
from mnectar.library.view import Randomized, RandomGroup, Grouped, Shifted
from mnectar.registry import Registry

def setup_module(module):
    import logging
    module.logger = logging.getLogger("mnectar.library.view")
    module.logger_old_level = logger.level
    module.logger.setLevel("DEBUG3")


def teardown_module(module):
    module.logger.setLevel(module.logger_old_level)


def test_view_app_chaining(app):
    with pytest.raises(TypeError):
        view = View([])

    with pytest.raises(TypeError):
        view = Sorted(Filtered([]))

    view = Sorted(Filtered([], app))

    assert view.app == app


def test_view_count(library_factory, app):
    library = library_factory()
    records = library.records
    view = View(records, app)

    for ix in range(len(view)):
        assert view.count(view[ix]) == 1
        assert view.count(view[ix].mrl) == 1


def test_view_base(library_factory, app):
    library = library_factory()
    records = library.records
    view = View(records, app)

    assert len(view) == len(records)
    for ix in range(len(view)):
        assert records[ix] == view[ix]

    # Verify calling the default update  method doesn't change anything ...
    view.update()
    assert len(view) == len(records)
    for ix in range(len(view)):
        assert records[ix] == view[ix]


def test_view_sorted_default(library_factory, app):
    library = library_factory()
    records = library.records
    view = Sorted(records, app)
    column = app.columns[0].name

    view.sort()
    for ix in range(1,len(view)):
        assert view[ix-1][column] <= view[ix][column]


def test_view_sorted(library_factory, app):
    library = library_factory()
    records = library.records
    view = Sorted(records, app)

    view.sort('artist')
    for ix in range(1,len(view)):
        assert view[ix-1]['artist'] <= view[ix]['artist']
        if view[ix-1]['artist'] == view[ix]['artist']:
            assert view[ix-1]['album'] <= view[ix]['album']
        if view[ix-1]['album'] == view[ix]['album']:
            assert view[ix-1]['tracknumber'] + 1 == view[ix]['tracknumber']

    view.sort('artist', True)
    for ix in range(1,len(view)):
        assert view[ix-1]['artist'] >= view[ix]['artist']
        if view[ix-1]['artist'] == view[ix]['artist']:
            assert view[ix-1]['album'] >= view[ix]['album']
        if view[ix-1]['album'] == view[ix]['album']:
            assert view[ix-1]['tracknumber'] - 1 == view[ix]['tracknumber']

    view.sort('genre')
    for ix in range(1,len(view)):
        assert view[ix-1]['genre'] <= view[ix]['genre']

    # sorting by column number
    view.sort(app.columns.indexOfName('artist'))
    for ix in range(1,len(view)):
        assert view[ix-1]['artist'] <= view[ix]['artist']
        if view[ix-1]['artist'] == view[ix]['artist']:
            assert view[ix-1]['album'] <= view[ix]['album']
        if view[ix-1]['album'] == view[ix]['album']:
            assert view[ix-1]['tracknumber'] + 1 == view[ix]['tracknumber']


def test_view_filtered(library_factory, app):
    library = library_factory()
    records = library.records
    view = Filtered(records, app)

    for artist in library.artists:
        view.filter(f'artist="{artist}"')
        artists = {_['artist'] for _ in view}
        assert len(artists) == 1
        assert artist in artists

    for genre in library.genres:
        view.filter(f'genre="{genre}"')
        genres = {_['genre'] for _ in view}
        assert len(genres) == 1
        assert genre in genres


def test_view_sorted_filtered(library_factory, app):
    library = library_factory()
    records = library.records

    # Test chained sorted/filtered views multiple orders to ensure order does not change
    # the behavior (regardless of what makes sense from a logical usage perspective)
    view1 = Sorted(Filtered(records, app))
    view2 = Filtered(Sorted(records, app))

    for view in (view1,view2):
        view.sort('artist')

        for artist in library.artists:
            view.filter(f'artist="{artist}"')
            artists = {_['artist'] for _ in view}
            assert len(artists) == 1
            assert artist in artists

            for ix in range(1,len(view)):
                assert view[ix-1]['artist'] <= view[ix]['artist']
                if view[ix-1]['artist'] == view[ix]['artist']:
                    assert view[ix-1]['album'] <= view[ix]['album']
                if view[ix-1]['album'] == view[ix]['album']:
                    assert view[ix-1]['tracknumber'] + 1 == view[ix]['tracknumber']

        view.sort('genre')
        for artist in library.artists:
            view.filter(f'artist="{artist}"')
            artists = {_['artist'] for _ in view}
            assert len(artists) == 1
            assert artist in artists

            for ix in range(1,len(view)):
                assert view[ix-1]['genre'] <= view[ix]['genre']

        view.sort('artist')
        for genre in library.genres:
            view.filter(f'genre="{genre}"')
            genres = {_['genre'] for _ in view}
            assert len(genres) == 1
            assert genre in genres

            for ix in range(1,len(view)):
                assert view[ix-1]['artist'] <= view[ix]['artist']
                if view[ix-1]['artist'] == view[ix]['artist']:
                    assert view[ix-1]['album'] <= view[ix]['album']
                if view[ix-1]['album'] == view[ix]['album']:
                    assert view[ix-1]['tracknumber'] + 1 == view[ix]['tracknumber']


def test_view_getitem(library_factory, app):
    library = library_factory()
    records = library.records
    view = Sorted(Filtered(records, app))
    view.sort('artist', True)

    for doc in library:
        assert doc == view[doc['mrl']].record
        assert doc.doc_id == view[doc['mrl']].doc_id

    num_sliced = 5
    sliced = view[:num_sliced]
    for ix in range(num_sliced):
        assert sliced[ix] == view[ix]

    with pytest.raises(TypeError):
        view[1.34]

    with pytest.raises(IndexError):
        view['Invalid/Path']

    with pytest.raises(IndexError):
        view[len(view)]


def test_view_contains(library_factory, app):
    library = library_factory()
    records = library.records
    base = View(records, app)
    filtered = Filtered(base)
    view = Sorted(filtered)
    view.sort('artist', True)

    assert filtered in view
    assert base in view
    assert base in filtered

    for rec in records:
        assert rec.mrl in view
        assert rec in view


def test_view_index(library_factory, app):
    library = library_factory()
    records = library.records
    view = Sorted(Filtered(records, app))
    view.sort('artist', True)

    for ix in range(len(view)):
        assert view.index(view[ix]) == ix
        assert view.index(view[ix].mrl) == ix
        assert view.index(id(view[ix]), is_id=True) == ix

    with pytest.raises(ValueError):
        view.index('invalid mrl')

def test_view_editable_order(library_factory, app):
    library  = library_factory()
    records  = library.records

    with pytest.raises(ValueError):
        view = Editable(Sorted(Filtered(records, app)))

    with pytest.raises(ValueError):
        view = Sorted(Editable(Filtered(records, app)))


def test_view_editable_append(library_factory, app):
    library  = library_factory()
    records  = library.records
    content1 = []
    content2 = []
    view1    = Editable(content1, app)
    view2    = Sorted(Filtered(Editable(content2, app)))

    view1 = view1
    for ix in range(len(records)):
        assert len(view1) == ix
        view1.append(records[ix])
        assert len(view1) == ix+1
        assert view1[-1] == records[ix]

    view2.sort('artist', True)
    view2.filter('tracknumber < 5')
    for ix in range(len(records)):
        assert len(view2) <= ix
        view2.append(records[ix])
        assert len(view2) <= ix+1

        for ix in range(1,len(view2)):
            assert view2[ix-1]['artist'] >= view2[ix]['artist']
            if view2[ix-1]['artist'] == view2[ix]['artist']:
                assert view2[ix-1]['album'] >= view2[ix]['album']
            if view2[ix-1]['album'] == view2[ix]['album']:
                assert view2[ix-1]['tracknumber'] - 1 == view2[ix]['tracknumber']
            assert view2[ix]['tracknumber'] < 5
    view2.filter("")
    assert len(view2) == len(records)


def test_view_editable_extend(library_factory, app):
    library  = library_factory()
    records  = library.records
    content1 = []
    content2 = []
    view1    = Editable(content1, app)
    view2    = Sorted(Filtered(Editable(content2, app)))

    assert len(view1) == 0
    view1.extend(records)
    assert len(view1) == len(records)
    for ix in range(len(records)):
        assert view1[ix] == records[ix]

    view2.sort('artist', True)
    view2.filter('tracknumber < 5')
    assert len(view2) == 0
    view2.extend(records)
    assert len(view2) <= len(records)
    for ix in range(len(records)):
        for ix in range(1,len(view2)):
            assert view2[ix-1]['artist'] >= view2[ix]['artist']
            if view2[ix-1]['artist'] == view2[ix]['artist']:
                assert view2[ix-1]['album'] >= view2[ix]['album']
            if view2[ix-1]['album'] == view2[ix]['album']:
                assert view2[ix-1]['tracknumber'] - 1 == view2[ix]['tracknumber']
            assert view2[ix]['tracknumber'] < 5
    view2.filter("")
    assert len(view2) == len(records)


def test_view_editable_insert(library_factory, app):
    library  = library_factory()
    records  = library.records
    content1 = []
    content2 = []
    view1    = Editable(content1, app)
    view2    = Sorted(Filtered(Editable(content2, app)))

    for ix in range(len(records)):
        assert len(view1) == ix
        view1.insert(0, records[ix])
        assert len(view1) == ix+1
        assert view1[0] == records[ix]

    view2.sort('artist', True)
    view2.filter('tracknumber < 5')
    for ix in range(len(records)):
        assert len(view2) <= ix
        view2.insert(0, records[ix])
        assert len(view2) <= ix+1

        for ix in range(1,len(view2)):
            assert view2[ix-1]['artist'] >= view2[ix]['artist']
            if view2[ix-1]['artist'] == view2[ix]['artist']:
                assert view2[ix-1]['album'] >= view2[ix]['album']
            if view2[ix-1]['album'] == view2[ix]['album']:
                assert view2[ix-1]['tracknumber'] - 1 == view2[ix]['tracknumber']
            assert view2[ix]['tracknumber'] < 5
    view2.filter("")
    assert len(view2) == len(records)


def test_view_editable_insert_random(library_factory, app):
    library  = library_factory()
    records  = library.records
    content1 = []
    content2 = []
    view1    = Editable(content1, app)

    for ix in range(len(records)):
        loc = random.choice(range(-1,len(view1)+10))
        idx = min(loc,len(view1))
        if idx == -1:
            idx = max(0, len(view1)-1)

        assert len(view1) == ix
        view1.insert(loc, records[ix])
        assert len(view1) == ix+1
        assert view1[idx] == records[ix]


def test_view_editable_remove_record(library_factory, app):
    library  = library_factory()
    records  = library.records
    content1 = []
    content2 = []
    view1    = Editable(content1, app)
    view2    = Sorted(Filtered(Editable(content2, app)))

    view1.extend(records)
    while len(view1) > 0:
        rec = view1[random.randrange(0, len(view1))]
        assert rec in view1
        view1.remove(rec)
        assert rec not in view1

    view2.sort('artist', True)
    view2.filter('tracknumber < 5')
    view2.extend(records)
    for ix in range(len(view2)):
        rec = view2[random.randrange(0, len(view2))]
        if rec['tracknumber'] < 5:
            assert rec in view2
        else:
            assert rec not in view2
        view2.remove(rec)
        assert rec not in view2

        for ix in range(1,len(view2)):
            assert view2[ix-1]['artist'] >= view2[ix]['artist']
            if view2[ix-1]['artist'] == view2[ix]['artist']:
                assert view2[ix-1]['album'] >= view2[ix]['album']
            assert view2[ix]['tracknumber'] < 5

    assert len(view2) == 0
    view2.filter("")
    assert len(view2) > 0

    for ix in range(1,len(view2)):
        assert view2[ix]['tracknumber'] >= 5
        assert view2[ix-1]['artist'] >= view2[ix]['artist']
        if view2[ix-1]['artist'] == view2[ix]['artist']:
            assert view2[ix-1]['album'] >= view2[ix]['album']


def test_view_editable_remove_mrl(library_factory, app):
    library  = library_factory()
    records  = library.records
    content1 = []
    content2 = []
    view1    = Editable(content1, app)
    view2    = Sorted(Filtered(Editable(content2, app)))

    view1.extend(records)
    while len(view1) > 0:
        rec = view1[random.randrange(0, len(view1))]
        view1.remove(rec.mrl)
        assert rec.mrl not in view1

    view2.sort('artist', True)
    view2.filter('tracknumber < 5')
    view2.extend(records)
    for ix in range(len(view2)):
        rec = view2[random.randrange(0, len(view2))]
        if rec['tracknumber'] < 5:
            assert rec in view2
        else:
            assert rec not in view2
        view2.remove(rec.mrl)
        assert rec.mrl not in view2

        for ix in range(1,len(view2)):
            assert view2[ix-1]['artist'] >= view2[ix]['artist']
            if view2[ix-1]['artist'] == view2[ix]['artist']:
                assert view2[ix-1]['album'] >= view2[ix]['album']
            assert view2[ix]['tracknumber'] < 5

    assert len(view2) == 0
    view2.filter("")
    assert len(view2) > 0

    for ix in range(1,len(view2)):
        assert view2[ix]['tracknumber'] >= 5
        assert view2[ix-1]['artist'] >= view2[ix]['artist']
        if view2[ix-1]['artist'] == view2[ix]['artist']:
            assert view2[ix-1]['album'] >= view2[ix]['album']


def test_view_editable_pop(library_factory, app):
    library  = library_factory()
    records  = library.records
    content1 = []
    content2 = []
    view1    = Editable(content1, app)
    view2    = Sorted(Filtered(Editable(content2, app)))

    view1.extend(records)
    while len(view1) > 0:
        ix = random.randrange(0, len(view1))
        rec = view1[ix]
        assert rec in view1
        popped = view1.pop(ix)
        assert popped == rec
        assert rec not in view1

    view2.sort('artist', True)
    view2.filter('tracknumber < 5')
    view2.extend(records)
    for ix in range(len(view2)):
        ix = random.randrange(0, len(view2))
        rec = view2[ix]
        if rec['tracknumber'] < 5:
            assert rec in view2
        else:
            assert rec not in view2
        popped = view2.pop(ix)
        assert popped == rec
        assert rec not in view2

        for ix in range(1,len(view2)):
            assert view2[ix-1]['artist'] >= view2[ix]['artist']
            if view2[ix-1]['artist'] == view2[ix]['artist']:
                assert view2[ix-1]['album'] >= view2[ix]['album']
            assert view2[ix]['tracknumber'] < 5

    assert len(view2) == 0
    view2.filter("")
    assert len(view2) > 0

    for ix in range(1,len(view2)):
        assert view2[ix]['tracknumber'] >= 5
        assert view2[ix-1]['artist'] >= view2[ix]['artist']
        if view2[ix-1]['artist'] == view2[ix]['artist']:
            assert view2[ix-1]['album'] >= view2[ix]['album']


def test_view_pointer_indexing(library_factory, app):
    library  = library_factory()
    records  = library.records
    view     = View(records, app)

    for ix in range(len(records)):
        pointer = view.pointer(ix)

        assert pointer.view        == view
        assert pointer.view_index  == ix
        assert pointer.order_index == ix
        assert pointer.id          == id(view[ix])
        assert pointer.mrl         == view[ix].mrl
        assert pointer.record      == view[ix]
        assert pointer.order       == view

        pointer = view.pointer(view[ix].mrl)

        assert pointer.view        == view
        assert pointer.view_index  == ix
        assert pointer.order_index == ix
        assert pointer.id          == id(view[ix])
        assert pointer.mrl         == view[ix].mrl
        assert pointer.record      == view[ix]
        assert pointer.order       == view

        pointer = view.pointer(pointer)

        assert pointer.view        == view
        assert pointer.view_index  == ix
        assert pointer.order_index == ix
        assert pointer.id          == id(view[ix])
        assert pointer.mrl         == view[ix].mrl
        assert pointer.record      == view[ix]
        assert pointer.order       == view

def test_view_pointer_indexing_errors(library_factory, app):
    library  = library_factory()
    records  = library.records
    view     = View(records, app)

    pointer = view.pointer("invalid_mrl")

    assert not pointer.valid
    assert pointer.view_index is None
    assert pointer.order_index is None


def test_view_pointer_frozen(library_factory, app):
    library  = library_factory()
    records  = library.records
    view     = View(records, app)
    pointer  = view.pointer(3)

    with pytest.raises(FrozenInstanceError):
        pointer.id = 123
    with pytest.raises(FrozenInstanceError):
        pointer.mrl = 'new mrl'
    with pytest.raises(FrozenInstanceError):
        pointer.view_index = 5


def test_view_pointer_next_prev_noloop(library_factory, app, setting):
    library  = library_factory()
    records  = library.records
    view     = View(records, app)

    pointer = view.pointer(0)

    with setting('playback.loop', False) as cm:
        for ix in range(len(records)):
            assert pointer.app   == app
            assert pointer.view  == view
            assert pointer.view_index == ix
            assert pointer.id    == id(view[ix])
            assert pointer.mrl   == view[ix].mrl

            pointer = pointer.next

            if ix == len(records)-1:
                assert not pointer.valid
                break

        assert not pointer.valid

        pointer = view.pointer(len(records)-1)

        for ix in reversed(range(len(records))):
            assert pointer.app   == app
            assert pointer.view  == view
            assert pointer.view_index == ix
            assert pointer.id    == id(view[ix])
            assert pointer.mrl   == view[ix].mrl

            pointer = pointer.prev
            if ix == 0:
                assert not pointer.valid
                break
        assert not pointer.valid


def test_view_pointer_next_prev_loop(library_factory, app, setting):
    library  = library_factory()
    records  = library.records
    view     = View(records, app)

    pointer = None

    with setting('playback.loop', True) as cm:
        for ix in range(len(records)):
            if pointer is None:
                pointer = view.pointer(ix)
            else:
                pointer = pointer.next

            assert pointer.app   == app
            assert pointer.view  == view
            assert pointer.view_index == ix
            assert pointer.id    == id(view[ix])
            assert pointer.mrl   == view[ix].mrl

        pointer = pointer.next
        assert pointer.view_index == 0
        pointer = None

        for ix in reversed(range(len(records))):
            if pointer is None:
                pointer = view.pointer(ix)
            else:
                pointer = pointer.prev

            assert pointer.app   == app
            assert pointer.view  == view
            assert pointer.view_index == ix
            assert pointer.id    == id(view[ix])
            assert pointer.mrl   == view[ix].mrl

        pointer = pointer.prev
        assert pointer.view_index == len(view)-1


def test_view_pointer_to_chain(library_factory, app, setting):
    library  = library_factory()
    records  = library.records
    view     = Filtered(Sorted(records, app))

    with setting('playback.loop', False):
        pointer = view.pointer(0)
        assert pointer.record == view[0]

        view.filter(f"album={view[0]['album']}")

        ix = 0
        while True:
            assert pointer.record['album'] == view[0]['album']
            pointer = pointer.next
            if not pointer.valid:
                break
            ix += 1
            if ix > len(records):
                assert False # Error occurred

        assert ix == len(view)-1

        view.filter(f"")
        pointer = view.pointer(0)
        assert pointer.record == view[0]

        view.filter(f"album={view[-1]['album']}")
        assert not pointer.valid
        assert pointer.record is None

        assert not pointer.next.valid
        assert not pointer.prev.valid

    with setting('playback.loop', True):
        view.filter(f"")
        assert pointer.valid

        view.sort('album', False)
        pointer = view.pointer(0)
        assert pointer.next == view.pointer(1)
        assert pointer.prev == view.pointer(len(view)-1)

        pointer = view.pointer(0)
        view.sort('album', True)
        assert pointer.view_index == len(view)-1
        assert pointer.next == view.pointer(0)
        assert pointer.prev == view.pointer(len(view)-2)

def test_view_changed(library_factory, app, qtbot):
    library = library_factory()
    records = library.records
    view    = Changed(Filtered(Sorted(records, app)))

    with qtbot.waitSignal(view.changed):
        view.sort('title')

    with qtbot.waitSignal(view.changed):
        view.filter('tracknumber <= 5')


def test_view_index_convert_kwargs(library_factory, app):
    class TestView(View, registry=Registry.Playlist):
        @View.action
        @View.index_convert('index')
        def func(self, *, index=None):
            return self[index]

    library = library_factory()
    records = library.records

    view_test   = TestView(records, app)
    view_sorted = Sorted(view_test)

    view_sorted.sort('album', True)
    assert view_test[0] != view_sorted[0]
    assert view_test[0] == view_test.func(index=0)
    assert view_sorted.func(index=0) == view_test[view_sorted[0].mrl]


def test_view_index_convert_args(library_factory, app):
    class TestView(View, registry=Registry.Playlist):
        @View.action
        @View.index_convert('index')
        def func(self, index):
            return self[index]

    library = library_factory()
    records = library.records

    view_test   = TestView(records, app)
    view_sorted = Sorted(view_test)

    view_sorted.sort('album', True)
    assert view_test[0] != view_sorted[0]
    assert view_test[0] == view_test.func(index=0)
    assert view_sorted.func(index=0) == view_test[view_sorted[0].mrl]

    assert view_test[0] != view_sorted[0]
    assert view_test[0] == view_test.func(0)
    assert view_sorted.func(0) == view_test[view_sorted[0].mrl]


def test_view_index_convert_edge(library_factory, app):
    class TestView(View, registry=Registry.Playlist):
        @View.action
        @View.index_convert('index')
        def func(self, index):
            return self[index]

    library = library_factory()
    records = library.records

    view_test   = TestView(records, app)
    view_sorted = Sorted(view_test)

    view_sorted.sort('album', True)
    assert view_test[-1] != view_sorted[-1]
    assert view_test[-1] == view_test.func(-1)
    assert view_sorted.func(-1) == view_test[view_sorted[-1].mrl]

    with pytest.raises(IndexError):
        view_sorted.func(len(view_sorted))

    view_sorted.sort('album', True)
    assert view_test[-1] != view_sorted[-1]
    assert view_test[-1] == view_test.func(index=-1)
    assert view_sorted.func(index=-1) == view_test[view_sorted[-1].mrl]

    with pytest.raises(IndexError):
        view_sorted.func(index=len(view_sorted))


def test_view_index_convert_invalid_name(library_factory, app):
    with pytest.raises(ValueError):
        class TestView(View, registry=Registry.Playlist):
            @View.action
            @View.index_convert('foo')
            def func(self, index):
                return self[index]

def test_view_editable_invalid_after_remove(library_factory, app):
    library  = library_factory()
    records  = library.records
    content1 = []
    view1    = Editable(content1, app)

    view1.append(records[0])

    assert len(view1) == 1

    pointer = view1.pointer(0)

    assert pointer.valid
    assert not pointer.next.valid

    view1.pop(0)

    assert not pointer.valid
    assert not pointer.next.valid

def test_view_pointer_invalid_behavior(app):
    pointer = ViewPointer(app)
    assert not pointer.valid
    assert not pointer.prev.valid
    assert not pointer.next.valid

def test_view_randomized(library_factory, app):
    library = library_factory()
    records = library.records
    view = Randomized(records, app)
    assert not all(old == new for old,new in zip(records,view))
    old_random = list(view)
    view.randomize()
    assert len(view) == len(old_random)
    assert len(view) == len(records)
    assert not all(old == new for old,new in zip(old_random,view))

def test_view_duplicate_chaining_and_dumb_sorted(library_factory, app):
    # NOTE: This tests both the behavior of chaining together duplicate views (the new
    # view takes priority for all actions) and dumb-sort behavior.  This has been chosen
    # because it is the most likey duplicate scenario.

    library  = library_factory()
    records  = library.records
    bytitle  = Sorted(records, app)
    byartist = Sorted(bytitle)

    bytitle.sort('title', smart=False)
    for ix in range(1, len(bytitle)):
        assert bytitle[ix-1]['title'] <= bytitle[ix]['title']

    byartist.sort('artist', smart=False)
    for ix in range(1,len(byartist)):
        assert byartist[ix-1]['artist'] <= byartist[ix]['artist']
        if byartist[ix-1]['artist'] == byartist[ix]['artist']:
            assert byartist[ix-1]['title']  <= byartist[ix]['title']


def test_view_grouped_str(library_factory, app):
    library  = library_factory()
    records  = library.records
    bytitle  = Sorted(records, app)
    byartist = Grouped(bytitle)

    bytitle.sort('title', smart=False)

    for ix in range(1, len(bytitle)):
        assert bytitle[ix-1]['title'] <= bytitle[ix]['title']

    artists = list({_['artist']: None for _ in bytitle}.keys())
    byartist.group('artist')
    for ix in range(1,len(byartist)):
        if byartist[ix-1]['artist'] != byartist[ix]['artist']:
            artists.pop(0)
        else:
            assert byartist[ix]['artist'] == artists[0]
            assert byartist[ix-1]['title']  <= byartist[ix]['title']

    artists = list({_['artist']: None for _ in bytitle}.keys())
    byartist.group()
    for ix in range(1,len(byartist)):
        if byartist[ix-1]['artist'] != byartist[ix]['artist']:
            artists.pop(0)
        else:
            assert byartist[ix]['artist'] == artists[0]
            assert byartist[ix-1]['title']  <= byartist[ix]['title']

    byartist.group("")
    for ix in range(1, len(byartist)):
        assert byartist[ix-1]['title'] <= byartist[ix]['title']

def test_view_grouped_year(library_factory, app):
    library  = library_factory()
    records  = library.records
    bytitle  = Sorted(records, app)
    view     = Grouped(bytitle)

    bytitle.sort('title', smart=False)

    for ix in range(1, len(bytitle)):
        assert bytitle[ix-1]['title'] <= bytitle[ix]['title']

    years = list({_['year']: None for _ in bytitle}.keys())
    view.group('year')
    for ix in range(1,len(view)):
        if view[ix-1]['year'] != view[ix]['year']:
            years.pop(0)
        else:
            assert view[ix]['year'] == years[0]
            assert view[ix-1]['title'] <= view[ix]['title']

def test_view_randomize_grouped_str(library_factory, app):
    library  = library_factory()
    records  = library.records
    bytitle  = Sorted(records, app)
    byartist = RandomGroup(bytitle)

    bytitle.sort('title', smart=False)
    artists = list({_['artist']: None for _ in bytitle}.keys())

    for ix in range(1, len(bytitle)):
        assert bytitle[ix-1]['title'] <= bytitle[ix]['title']

    byartist.randomize_group('artist')
    randomized1 = list({_['artist']: None for _ in byartist}.keys())
    assert randomized1 != artists
    for ix in range(1,len(byartist)):
        if byartist[ix-1]['artist'] != byartist[ix]['artist']:
            randomized1.pop(0)
        else:
            assert byartist[ix]['artist'] == randomized1[0]
            assert byartist[ix-1]['title']  <= byartist[ix]['title']

    byartist.randomize_group()
    randomized2 = list({_['artist']: None for _ in byartist}.keys())
    assert randomized2 != artists
    assert randomized2 != randomized1
    for ix in range(1,len(byartist)):
        if byartist[ix-1]['artist'] != byartist[ix]['artist']:
            randomized2.pop(0)
        else:
            assert byartist[ix]['artist'] == randomized2[0]
            assert byartist[ix-1]['title']  <= byartist[ix]['title']

    byartist.randomize_group("")
    for ix in range(1, len(byartist)):
        assert byartist[ix-1]['title'] <= byartist[ix]['title']


def test_view_random_group_invalid(library_factory, app):
    library   = library_factory()
    records   = library.records
    bytitle   = Sorted(records, app)
    bymissing = RandomGroup(bytitle)

    bytitle.sort('title', smart=False)
    artists = list({_['artist']: None for _ in bytitle}.keys())

    for ix in range(1, len(bytitle)):
        assert bytitle[ix-1]['title'] <= bytitle[ix]['title']

    bymissing.randomize_group('missing')
    randomized1 = list({_['artist']: None for _ in bymissing}.keys())
    assert randomized1 == artists
    for ix in range(1, len(bytitle)):
        assert bymissing[ix-1]['title'] <= bymissing[ix]['title']


def test_view_pointer_ordered(library_factory, app, setting):
    library = library_factory()
    records = library.records
    bytitle = Sorted(records, app)

    bytitle.sort('title')

    pointer = bytitle.pointer(0)
    byartist = Grouped(pointer.view)
    byartist.group('artist')
    ordered = pointer.reorder(byartist)

    assert ordered.valid
    assert pointer.id    == ordered.id
    assert pointer.mrl   == ordered.mrl
    assert pointer.view  == bytitle
    assert pointer.view  == ordered.view
    assert ordered.view  == bytitle
    assert ordered.order == byartist

    for ix in range(1, len(bytitle)):
        assert bytitle[ix-1]['title'] <= bytitle[ix]['title']

    artists = list({_['artist']: None for _ in bytitle}.keys())
    byartist.group()
    for ix in range(1,len(byartist)):
        if byartist[ix-1]['artist'] != byartist[ix]['artist']:
            artists.pop(0)
        else:
            assert byartist[ix]['artist'] == artists[0]
            assert byartist[ix-1]['title']  <= byartist[ix]['title']

    with setting('playback.loop', False) as cm:
        artists = list({_['artist']: None for _ in bytitle}.keys())
        while artists[0] != ordered.record['artist']:
            artists.pop(0)

        assert not ordered.loop
        previous = ordered
        ordered = ordered.next
        while ordered.valid:
            if previous.record['artist'] != ordered.record['artist']:
                artists.pop(0)

            assert ordered.mrl in ordered.view
            assert ordered.record['artist'] == artists[0]
            if previous.record['artist'] == ordered.record['artist']:
                assert previous.record['title'] <= ordered.record['title']

            previous = ordered
            ordered = ordered.next


def test_view_pointer_ordered_invalid(library_factory, app, setting):
    library = library_factory()
    records = library.records
    view1 = Sorted(records, app)
    view2 = Filtered(records, app)
    view3 = Filtered(view1)
    view1.sort('artist')

    pointer1 = view1.pointer(0)
    pointer2 = pointer1.reorder(view2)
    assert not pointer2.valid

    pointer3 = view3.pointer(0)
    pointer4 = pointer3.reorder(view1)
    assert not pointer4.valid

    pointer5 = view3.pointer(len(view1))
    pointer6 = pointer5.reorder(view3)
    assert not pointer5.valid
    assert not pointer6.valid

    with setting('playback.loop', False) as cm:
        pointer7 = view1.pointer(len(view1)-1)
        pointer8 = pointer7.reorder(view3)
        assert pointer7.valid
        assert pointer8.valid
        view3.filter(f"artist = {view1[0]['artist']}")
        assert pointer7.valid
        assert not pointer8.valid
        view3.filter(f"artist = Invalid Artist")
        pointer10 = pointer7.reorder(view3)
        assert not pointer10.valid

    with setting('playback.loop', True) as cm:
        view3.filter(f"artist = {view1[0]['artist']}")
        pointer9 = pointer7.reorder(view3)
        assert pointer9.valid
        assert pointer9.view_index == 0


def test_view_indexing_with_pointer(library_factory, app):
    library = library_factory()
    records = library.records
    view    = View(records, app)
    pointer = view.pointer(0)

    assert view[pointer] == pointer.record
    assert view.index(pointer) == pointer.view_index
    assert pointer in view
    assert view.count(pointer) == 1

    with pytest.raises(IndexError):
        view[view.pointer(len(view))]

def test_view_shifted_pointer(library_factory, app):
    library = library_factory()
    records = library.records
    view    = Sorted(records, app, default=('title'))
    pointer = view.pointer(len(view)//2)
    shifted = Shifted(view, default=pointer)

    assert len(view) == len(shifted)
    assert pointer.view_index != 0
    assert pointer.view_index != (len(view)-1)
    assert pointer.record == view[pointer.view_index]
    assert pointer.record == shifted[0]

    for ix in range(len(view)):
        assert shifted[ix] == view[(ix+pointer.view_index)%len(view)]


def test_view_shifted_int(library_factory, app):
    library = library_factory()
    records = library.records
    view    = Sorted(records, app, default=('title'))
    pointer = view.pointer(len(view)//2)
    shifted = Shifted(view, default=pointer.view_index)

    assert len(view) == len(shifted)
    assert pointer.view_index != 0
    assert pointer.view_index != (len(view)-1)
    assert pointer.record == view[pointer.view_index]
    assert pointer.record == shifted[0]

    for ix in range(len(view)):
        assert shifted[ix] == view[(ix+pointer.view_index)%len(view)]


def test_view_shifted_mrl(library_factory, app):
    library = library_factory()
    records = library.records
    view    = Sorted(records, app, default=('title'))
    pointer = view.pointer(len(view)//2)
    shifted = Shifted(view, default=pointer.mrl)

    assert len(view) == len(shifted)
    assert pointer.view_index != 0
    assert pointer.view_index != (len(view)-1)
    assert pointer.record == view[pointer.view_index]
    assert pointer.record == shifted[0]

    for ix in range(len(view)):
        assert shifted[ix] == view[(ix+pointer.view_index)%len(view)]

def test_view_shifted_errors(library_factory, app):
    library = library_factory()
    records = library.records
    view    = Sorted(records, app, default=('title'))

    shifted = Shifted(view, default=-1)
    for ix in range(len(view)):
        assert shifted[ix] == view[ix]

    shifted = Shifted(view, default="invalid_mrl")
    for ix in range(len(view)):
        assert shifted[ix] == view[ix]

    shifted = Shifted(records, app, default="mrl_access_is_invalid")
    for ix in range(len(view)):
        assert shifted[ix] == records[ix]


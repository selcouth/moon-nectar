import logging
import pytest
import random

from mnectar.library.TinydbStorage import LibraryRecordTinyDB
from mnectar.library.TinydbStorage import LibraryContentTinyDB

def setup_module(module):
    module.logger = logging.getLogger('mnectar.library.TinydbStorage')
    module.logger_old_level = logger.level
    module.logger.setLevel('DEBUG3')

def teardown_module(module):
    module.logger.setLevel(module.logger_old_level)

@pytest.fixture
def record():
    """Test Record Generator"""

    class generator:
        key_auto = 9100
        def __call__(self, key=None):
            if key == None:
                key  = self.key_auto
                self.key_auto += 1

            return {'mrl':            str(key),
                    'field_int':      key//10,
                    'field_float':    key/10,
                    'field_str':      "foo",
                    'field_str_2':    f"foo {key}",
                    'field_list_str': ["foo", "bar"],
                    'field_list_int': [random.randint(0,1000), random.randint(0,1000)]}

    return generator()

@pytest.fixture
def content(tmp_path):
    filename = tmp_path/'tinydb_test_library.json'
    print(f'Library File: {filename}')
    library = LibraryContentTinyDB(filename)
    yield library
    library.flush()

@pytest.mark.incremental
class TestTinydbStorage:
    def test_library_insert(self, record, content):
        records = [record() for _ in range(50)]

        for _ in records: content.update(_, content.query.mrl == _['mrl'])

        content.flush()
        content.read(content.filename)

        assert len(content) == len(records)

    def test_library_open(self, content):
        assert content.filename.exists()
        assert len(content) == 0

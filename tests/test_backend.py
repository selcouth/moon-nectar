import logging
import os
import pytest
import mnectar

from mnectar.library.view import View, ViewPointer, Sorted
from mnectar.registry     import Registry
import Registry.Backend.BackendMock
import Registry.Backend.BackendVLC

def setup_module(module):
    module.logger = logging.getLogger("mnectar.backend")
    module.logger_old_level = logger.level
    module.logger.setLevel("DEBUG3")

    module.logger_signal = logging.getLogger("mnectar.util.signal.")
    module.logger_old_level_signal = logger.level
    module.logger_signal.setLevel("DEBUG3")

    import Registry.Backend.BackendMock


def teardown_module(module):
    module.logger.setLevel(module.logger_old_level)
    module.logger_signal.setLevel(module.logger_old_level_signal)


@pytest.fixture
def skipIfGitLab():
    pytest.mark.skipif(
        "GITLAB_CI" in os.environ,
        reason="VLC Backend cannot be tested on Gitlab CI because",
    )


tests = [
    (Registry.Backend.BackendMock, False, False),
    (Registry.Backend.BackendVLC, False, True),
    (Registry.Backend.BackendVLC, True, False),
]


@pytest.mark.parametrize(
    "cls, skipGitLab, vlcmockif", tests, indirect=["skipGitLab", "vlcmockif"]
)
def test_backend_enable_disable(cls, skipGitLab, vlcmockif, app):
    backend = cls(app)

    # Verify the backend starts disabled
    assert not backend.enabled

    # Verify a repeat disable does not cause problems
    backend.disable()
    assert not backend.enabled

    # Verify enable and repeat enable does not cause problems
    backend.enable()
    assert backend.enabled
    backend.enable()
    assert backend.enabled

    # Verify disable-eanble cycle
    backend.disable()
    assert not backend.enabled
    backend.enable()
    assert backend.enabled

    # Cleanup: disable
    backend.disable()
    assert not backend.enabled


@pytest.mark.parametrize(
    "cls, skipGitLab, vlcmockif", tests, indirect=["skipGitLab", "vlcmockif"]
)
def test_backend_signals(cls, skipGitLab, vlcmockif, app, libfile, qtbot):
    # Test Setup: Get a valid MRL to play
    app.library.dbLoad(libfile)
    view = View(app.library.content.records, app)
    pointer = view.pointer(0)
    print(f"Test File: {pointer.mrl}")

    # Test Setup: Configure the backend
    backend = cls(app)
    backend.enable()
    assert backend.enabled

    # Test: mute
    with qtbot.waitSignal(app.signal.muted) as sig:
        app.signal.mute.emit(True)
    assert sig.args == [True]

    # Test: Play signal with no MRL configured
    with qtbot.waitSignal(app.signal.playlistEnd) as sig:
        app.signal.play.emit()

    # Test: Play a file
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.playNew.emit(pointer)
    assert len(sig.args) == 2
    assert sig.args[0] == pointer
    assert isinstance(sig.args[1], float)

    # Test: Position updates and seek
    # NOTE: This is a coarse test "did a seek operation occur"?
    #       ... because the exact timing of position updates is unknown
    # ... Because position updates regularly, best to clear the signal before waiting!
    app.signal.seek.emit(0.5)
    with qtbot.waitSignal(app.signal.position) as sig_pos:
        pass
    with qtbot.waitSignal(app.signal.time) as sig_time:
        pass
    assert sig_pos.args[0] >= 0.5

    oldtime = sig_time.args[0]
    app.signal.seek.emit(0.2)
    with qtbot.waitSignal(app.signal.position) as sig_pos:
        pass
    with qtbot.waitSignal(app.signal.time) as sig_time:
        pass
    assert 0.2 <= sig_pos.args[0] < 0.5
    assert sig_time.args[0] < oldtime

    # Test: pause via toggle
    with qtbot.waitSignal(app.signal.paused) as sig:
        app.signal.togglePause.emit()
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.togglePause.emit()
    assert len(sig.args) == 2
    assert sig.args[0] == pointer
    assert isinstance(sig.args[1], float)

    # Test: next file is playing (AFTER playEnd)
    with qtbot.waitSignal(app.signal.playing) as sig_playing:
        # Test: end of mrl signal
        with qtbot.waitSignal(app.signal.playEnd) as sig:
            app.signal.seek.emit(0.9999)
        sig.args == [pointer]

    assert len(sig_playing.args) == 2
    assert sig_playing.args[0] == pointer.next
    assert isinstance(sig_playing.args[1], float)

    pointer = pointer.next

    # Test: stop
    with qtbot.waitSignal(app.signal.stopped) as sig:
        app.signal.stop.emit()

    # Test: play of existing file
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.play.emit()
    assert len(sig.args) == 2
    assert sig.args[0] == pointer
    assert isinstance(sig.args[1], float)

    # Test: play also can cancel pause
    with qtbot.waitSignal(app.signal.paused) as sig:
        app.signal.pause.emit()
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.play.emit()
    assert len(sig.args) == 2
    assert sig.args[0] == pointer
    assert isinstance(sig.args[1], float)

    # Test: Play previous
    assert pointer.prev.valid
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.playPrev.emit()
    assert len(sig.args) == 2
    assert sig.args[0] == pointer.prev
    assert isinstance(sig.args[1], float)

    # Test playlist end on previous
    pointer = view.pointer(0)
    assert not pointer.prev.valid
    with qtbot.waitSignal(app.signal.playlistEnd) as sig:
        app.signal.playNew.emit(pointer)
        app.signal.playPrev.emit()
    assert sig.args[0].view == pointer.view

    # Verify playing previous also sends playlistEnd on an invalid pointer
    assert not pointer.prev.valid
    with qtbot.waitSignal(app.signal.playlistEnd) as sig:
        app.signal.playNew.emit(pointer.prev)
        app.signal.playPrev.emit()
    assert sig.args[0].view == pointer.view

    # Verify play next
    pointer = view.pointer(0)
    assert pointer.next.valid
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.playNew.emit(pointer)
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.playNext.emit()
    assert len(sig.args) == 2
    assert sig.args[0] == pointer.next
    assert isinstance(sig.args[1], float)

    # Test: playlist end emitted when there is no next item to play
    pointer = view.pointer(len(view)-1)
    assert not pointer.next.valid
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.playNew.emit(pointer)
    assert pointer == sig.args[0]
    with qtbot.waitSignal(app.signal.playlistEnd) as sig:
        app.signal.playNext.emit()

    # Test: playlist end emitted when there is no next item to play
    # ...natural rollover version
    pointer = view.pointer(len(view)-1)
    assert not pointer.next.valid
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.playNew.emit(pointer)
    assert pointer == sig.args[0]
    with qtbot.waitSignal(app.signal.playlistEnd) as sig:
        app.signal.seek.emit(0.9999)

    # Test: unmute
    # ... While playback is beginning, mute operations may not be valid (e.g. VLC)
    # ... So seek into the song
    # ... NOTE: This opens a tiny possibility of error, however playback startup should
    #           be so brief in real operations that this is not a concern.
    # ... Because position updates regularly, best to clear the signal before waiting!
    pointer = view.pointer(len(view)-1)
    assert not pointer.next.valid
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.playNew.emit(pointer)
    assert pointer == sig.args[0]
    with qtbot.waitSignal(app.signal.position) as sig:
        pass
    with qtbot.waitSignal(app.signal.position) as sig:
        pass

    with qtbot.waitSignal(app.signal.muted) as sig:
        app.signal.mute.emit(False)
    assert sig.args == [False]

    # Cleanup: disable
    backend.disable()
    assert not backend.enabled


@pytest.mark.parametrize(
    "cls, skipGitLab, vlcmockif", tests, indirect=["skipGitLab", "vlcmockif"]
)
def test_backend_signals_toggle_play_pause(
    cls, skipGitLab, vlcmockif, app, libfile, qtbot
):
    # Test Setup: Get a valid MRL to play
    app.library.dbLoad(libfile)
    mrl = app.library.content[1].mrl
    print(f"Test File: {mrl}")

    # Test Setup: Configure the backend
    backend = cls(app)
    backend.enable()
    assert backend.enabled

    # Test: mute
    with qtbot.waitSignal(app.signal.muted) as sig:
        app.signal.mute.emit(True)
    assert sig.args == [True]

    # Test toggle play/pause from stopped calls play next
    with qtbot.waitSignal(app.signal.playNext) as sig:
        app.signal.togglePause.emit()

    # Test: unmute
    with qtbot.waitSignal(app.signal.muted) as sig:
        app.signal.mute.emit(False)
    assert sig.args == [False]

    # Cleanup: disable
    backend.disable()
    assert not backend.enabled

@pytest.mark.parametrize(
    "cls, skipGitLab, vlcmockif", tests, indirect=["skipGitLab", "vlcmockif"]
)
def test_backend_function_interface(
    cls, skipGitLab, vlcmockif, app, libfile, qtbot
):
    """
    Test the function-based interface to the backend.
    This is a simple interface intended for testing only.
    Thus the below test is simple and may not cover all scenarios.
    """
    # Test Setup: Get a valid MRL to play
    app.library.dbLoad(libfile)
    view = View(app.library.content.records, app)
    pointer = view.pointer(0)
    print(f"Test File: {pointer.mrl}")

    # Test Setup: Configure the backend
    backend = cls(app)
    backend.enable()
    assert backend.enabled

    # Test play an mrl
    with qtbot.waitSignal(app.signal.playing) as sig:
        backend.play_new(pointer)

    # Test Mute
    with qtbot.waitSignal(app.signal.muted) as sig:
        backend.mute(True)
    assert sig.args == [True]

    # Test pause
    with qtbot.waitSignal(app.signal.paused) as sig:
        backend.pause()
    assert backend.is_paused()
    assert not backend.is_playing()

    backend.pause()
    assert backend.is_paused()
    assert not backend.is_playing()

    with qtbot.waitSignal(app.signal.playing) as sig:
        backend.play()
    assert backend.is_playing()
    assert not backend.is_paused()

    with qtbot.waitSignal(app.signal.paused) as sig:
        backend.togglePause()
    assert backend.is_paused()
    assert not backend.is_playing()

    with qtbot.waitSignal(app.signal.playing) as sig:
        backend.togglePause()
    assert backend.is_playing()
    assert not backend.is_paused()

    # Test set position
    # ... Because position updates regularly, best to clear the signal before waiting!
    with qtbot.waitSignal(app.signal.position) as sig_pos:
        pass
    with qtbot.waitSignal(app.signal.time) as sig_time:
        pass
    assert sig_pos.args[0] < 0.5

    backend.set_position(0.6)
    with qtbot.waitSignal(app.signal.position) as sig_pos:
        pass
    with qtbot.waitSignal(app.signal.time) as sig_time:
        pass
    assert sig_pos.args[0] >= 0.6

    # Test Unmute
    with qtbot.waitSignal(app.signal.muted) as sig:
        backend.mute(False)
    assert sig.args == [False]

    # Test stop
    with qtbot.waitSignal(app.signal.stopped) as sig:
        backend.stop()
    assert not backend.is_paused()
    assert not backend.is_playing()

    # Test play previous startup condition
    backend._pointer = None
    backend._pointer_after = None
    with qtbot.waitSignal(app.signal.playlistEnd) as sig:
        backend.play_prev()

    # Test play startup condition
    backend._pointer = None
    backend._pointer_after = None
    with qtbot.waitSignal(app.signal.playlistEnd) as sig:
        backend.play()

@pytest.mark.parametrize(
    "cls, skipGitLab, vlcmockif", tests, indirect=["skipGitLab", "vlcmockif"]
)
def test_backend_play_after(cls, skipGitLab, vlcmockif, app, libfile, qtbot):
    app.library.dbLoad(libfile)
    view = View(app.library.content.records, app)
    pointer = view.pointer(0)

    # Test Setup: Configure the backend
    backend = cls(app)
    backend.enable()
    assert backend.enabled

    # Test: mute
    with qtbot.waitSignal(app.signal.muted) as sig:
        app.signal.mute.emit(True)
    assert sig.args == [True]

    # Verify nothing starts when playAfter is emitted at app startup
    with qtbot.waitSignal(app.signal.playing, 500, raising=False) as sig:
        app.signal.playAfter.emit(pointer)
    assert not sig.signal_triggered, "process timed-out"

    assert not backend.is_playing()
    assert not backend.is_paused()

    # Verify nothing starts when playAfter is emitted at app startup
    # ... if an invalid play after is given
    with qtbot.waitSignal(app.signal.playlistEnd, 500) as sig:
        app.signal.playAfter.emit(ViewPointer(app, None))
        app.signal.play.emit()

    assert not backend.is_playing()
    assert not backend.is_paused()

    # Verify playback begins correctly on play next
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.playAfter.emit(pointer)
        app.signal.playNext.emit()

    assert sig.args[0] == pointer

    # Queue a new song and verify playback does not change
    pointer_new = view.pointer(len(view)-1)
    with qtbot.waitSignal(app.signal.stopped, 500, raising=False) as sig:
        app.signal.playAfter.emit(pointer_new)
    assert not sig.signal_triggered, "process timed-out"

    # Verify playback begins correctly on play next
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.playNext.emit()
    assert sig.args[0] == pointer_new

    # Verify end of song plays next correctly
    pointer_new = view.pointer(1)
    assert backend.is_playing()
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.playAfter.emit(pointer_new)
        app.signal.seek.emit(0.999999)
    assert sig.args[0] == pointer_new

    # Verify playlist end on play after invalid
    pointer = view.pointer(len(view)-1)
    assert not pointer.next.valid
    with qtbot.waitSignal(app.signal.playlistEnd) as sig:
        app.signal.playAfter.emit(pointer.next)
        app.signal.playNext.emit()

    # Test Unmute
    with qtbot.waitSignal(app.signal.muted) as sig:
        backend.mute(False)
    assert sig.args == [False]

    # Stop playback
    with qtbot.waitSignal(app.signal.stopped) as sig:
        backend.stop()
    assert not backend.is_paused()
    assert not backend.is_playing()


@pytest.mark.parametrize(
    "cls, skipGitLab, vlcmockif", tests, indirect=["skipGitLab", "vlcmockif"]
)
def test_backend_play_after_reorder(cls, skipGitLab, vlcmockif, app, libfile, qtbot):
    app.library.dbLoad(libfile)
    view = View(app.library.content.records, app)
    pointer = view.pointer(0)

    # Test Setup: Configure the backend
    backend = cls(app)
    backend.enable()
    assert backend.enabled

    # Test: mute
    with qtbot.waitSignal(app.signal.muted) as sig:
        app.signal.mute.emit(True)
    assert sig.args == [True]

    # Verify playback begins correctly on play next
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.playAfter.emit(pointer)
        app.signal.playNext.emit()

    assert sig.args[0] == pointer

    # Send a reorder and verify nothing changes
    reordered = pointer.reorder(Sorted(pointer.view, default='title'))
    assert pointer == reordered
    assert pointer.order != reordered.order
    assert pointer.next != reordered.next
    with qtbot.waitSignal(app.signal.stopped, 500, raising=False) as sig:
        app.signal.playAfter.emit(reordered)
    assert not sig.signal_triggered, "process timed-out"

    # Verify playback begins correctly on play next
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.playNext.emit()
    assert sig.args[0] == reordered.next

    # Test Unmute
    with qtbot.waitSignal(app.signal.muted) as sig:
        backend.mute(False)
    assert sig.args == [False]

    # Stop playback
    with qtbot.waitSignal(app.signal.stopped) as sig:
        backend.stop()
    assert not backend.is_paused()
    assert not backend.is_playing()

@pytest.mark.parametrize(
    "cls, skipGitLab, vlcmockif", tests, indirect=["skipGitLab", "vlcmockif"]
)
def test_backend_toggle_mute(cls, skipGitLab, vlcmockif, app, libfile, qtbot):
    app.library.dbLoad(libfile)
    view = View(app.library.content.records, app)
    pointer = view.pointer(0)

    # Test Setup: Configure the backend
    backend = cls(app)
    backend.enable()
    assert backend.enabled

    # Test: mute
    with qtbot.waitSignal(app.signal.muted) as sig:
        app.signal.mute.emit(True)
    assert sig.args == [True]

    # Test: mute toggle
    with qtbot.waitSignal(app.signal.muted) as sig:
        app.signal.toggleMute.emit()
    assert sig.args == [False]

    # Test: mute toggle
    with qtbot.waitSignal(app.signal.muted) as sig:
        app.signal.toggleMute.emit()
    assert sig.args == [True]

    # Test: mute
    with qtbot.waitSignal(app.signal.muted) as sig:
        app.signal.mute.emit(False)
    assert sig.args == [False]


@pytest.mark.parametrize(
    "cls, skipGitLab, vlcmockif", tests, indirect=["skipGitLab", "vlcmockif"]
)
def test_backend_skip(cls, skipGitLab, vlcmockif, app, libfile, qtbot):
    app.library.dbLoad(libfile)
    view = View(app.library.content.records, app)
    pointer = view.pointer(0)

    # Test Setup: Configure the backend
    backend = cls(app)
    backend.enable()
    assert backend.enabled

    # Test: mute
    with qtbot.waitSignal(app.signal.muted) as sig:
        app.signal.mute.emit(True)
    assert sig.args == [True]

    # Test: Play a file
    with qtbot.waitSignal(app.signal.playing) as sig:
        app.signal.playNew.emit(pointer)
    assert len(sig.args) == 2
    assert sig.args[0] == pointer
    assert isinstance(sig.args[1], float)

    # Test: Skip Forward
    with qtbot.waitSignal(app.signal.time) as sig_time:
        pass
    old_time = sig_time.args[0]

    app.signal.skip.emit(20)
    with qtbot.waitSignal(app.signal.time) as sig_time:
        pass
    new_time = sig_time.args[0]

    assert round(new_time - old_time) == 20

    # Test: Skip Backward
    with qtbot.waitSignal(app.signal.time) as sig_time:
        pass
    old_time = sig_time.args[0]

    app.signal.skip.emit(-10)
    with qtbot.waitSignal(app.signal.time) as sig_time:
        pass
    new_time = sig_time.args[0]

    assert round(new_time - old_time) == -10

    # Test: mute
    with qtbot.waitSignal(app.signal.muted) as sig:
        app.signal.mute.emit(False)
    assert sig.args == [False]

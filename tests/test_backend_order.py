import pytest

from mnectar.registry import Registry

from mnectar.library.view import Sorted, View, ViewPointer
from mnectar.backend.order import AgentBase, RandomArtist, RandomAlbum


def setup_module(module):
    import logging
    module.logger = logging.getLogger("mnectar.backend.order")
    module.logger_old_level = logger.level
    module.logger.setLevel("DEBUG3")


def teardown_module(module):
    module.logger.setLevel(module.logger_old_level)


def test_backend_order_agents(app, library_factory):
    order  = Registry.Control.OrderManager(app)
    agents = Registry.Control.OrderAgents.plugins

    # Verify registry content works as expected

    for agent in agents:
        assert agent in order.agents
        assert agent.__name__ in order.agent_names
        assert agent.__name__ in order.agent_dict
        assert order.agent_dict[agent.__name__] == agent

        order.agent = agent
        assert isinstance(order.agent, agent)

        order.agent = agent.__name__
        assert isinstance(order.agent, agent)


def test_backend_order_agent_default(app, library_factory, qtbot):
    order   = Registry.Control.OrderManager(app)
    library = library_factory()
    records = library.records
    view    = Sorted(records, app, default='title')
    pointer = view.pointer(0)
    order.enable()

    # Verify by default, nothing is touched

    assert order.agent

    with qtbot.waitSignal(app.signal.playAfter, 200, raising=False) as sig:
        app.signal.playing.emit(pointer, pointer.record['length'])
    assert not sig.signal_triggered, "process timed-out"

    with qtbot.waitSignal(app.signal.playAfter, 200) as sig:
        newptr = pointer.reorder(View(view))
        app.signal.playing.emit(newptr, newptr.record['length'])
    assert sig.args[0].view == sig.args[0].order


def test_backend_order_agent_enable_disable(app, library_factory, qtbot):
    order   = Registry.Control.OrderManager(app)
    library = library_factory()
    records = library.records
    view    = Sorted(records, app, default='title')
    pointer = view.pointer(0)

    with qtbot.waitSignal(app.signal.playAfter, 200, raising=False) as sig:
        newptr = pointer.reorder(View(view))
        app.signal.playing.emit(newptr, newptr.record['length'])
    assert not sig.signal_triggered, "process timed-out"

    order.enable()

    with qtbot.waitSignal(app.signal.playAfter, 200) as sig:
        newptr = pointer.reorder(View(view))
        app.signal.playing.emit(newptr, newptr.record['length'])
    assert sig.args[0].view == sig.args[0].order

    order.disable()

    with qtbot.waitSignal(app.signal.playAfter, 200, raising=False) as sig:
        newptr = pointer.reorder(View(view))
        app.signal.playing.emit(newptr, newptr.record['length'])
    assert not sig.signal_triggered, "process timed-out"


def test_backend_order_agent_set(app, library_factory, qtbot, setting):
    order   = Registry.Control.OrderManager(app)
    library = library_factory()
    records = library.records
    view    = Sorted(records, app, default='title')
    pointer = view.pointer(0)
    agents  = Registry.Control.OrderAgents.plugins
    order.agent = AgentBase
    order.enable()

    # Emit an initial signal to test the base setup ...
    with qtbot.waitSignal(app.signal.playAfter, 200, raising=False) as sig:
        app.signal.playing.emit(pointer, pointer.record['length'])
    assert not sig.signal_triggered, "process timed-out"

    # Verify registry content works as expected

    result_pointer = pointer
    with setting('playback.loop', True) as cm:
        for agent in reversed(agents):
            order.agent = AgentBase

            test_agent = agent(app)

            # Verify correct signals are sent on reorder while playing
            if test_agent.is_ordered(result_pointer):
                with qtbot.waitSignal(app.signal.playAfter, 200, raising=False) as sig:
                    order.agent = agent
                assert not sig.signal_triggered, "process timed-out"

            else:
                with qtbot.waitSignal(app.signal.playAfter, 200) as sig:
                    order.agent = agent
                result_pointer = sig.args[0]

                assert result_pointer.valid
                assert result_pointer.view == view
                assert order.agent.is_ordered(result_pointer)

            pointer = pointer.next

            # Verify correct signals are sent on new playing signal
            if order.agent.is_ordered(pointer):
                with qtbot.waitSignal(app.signal.playAfter, 200, raising=False) as sig:
                    app.signal.playing.emit(pointer, pointer.record['length'])
                assert not sig.signal_triggered, "process timed-out"

            else:
                with qtbot.waitSignal(app.signal.playAfter, 200) as sig:
                    app.signal.playing.emit(pointer, pointer.record['length'])
                result_pointer = sig.args[0]

                assert result_pointer.valid
                assert result_pointer.view == view
                assert order.agent.is_ordered(result_pointer)


    with qtbot.waitSignal(app.signal.playAfter, 200, raising=False) as sig:
        pointer = pointer.next
        app.signal.playlistEnd.emit(pointer)
        app.signal.playing.emit(pointer, pointer.record['length'])

def test_backend_order_is_ordered(app, library_factory):
    order   = Registry.Control.OrderManager(app)
    library = library_factory()
    records = library.records
    view    = Sorted(records, app, default='title')
    pointer = view.pointer(0)
    pointer = pointer.reorder(View(View(View(pointer.view))))
    order.enable()

    assert not Registry.Control.OrderAgents.Random(app).is_ordered(pointer)

    pointer = Registry.Control.OrderAgents.Random(app).reorder(pointer)
    pointer.order.shift(3)

    assert not Registry.Control.OrderAgents.Random(app).is_ordered(pointer)

def test_backend_order_swap(app, library_factory, qtbot):
    order   = Registry.Control.OrderManager(app)
    library = library_factory()
    records = library.records
    view    = Sorted(records, app, default='title')
    pointer = view.pointer(0)
    order.enable()

    with qtbot.waitSignal(app.signal.playAfter, 500) as sig:
        order.agent = 'Random'
        with qtbot.waitSignal(app.signal.playing, 200) as sig_playing:
            app.signal.playing.emit(pointer, pointer.record['length'])

    pointer=sig.args[0].next

    with qtbot.waitSignal(app.signal.playing, 200) as sig_playing:
        app.signal.playing.emit(pointer, pointer.record['length'])

    with qtbot.waitSignal(app.signal.playAfter, 200) as sig:
        order.agent = 'RandomAlbum'
    assert order.agent.is_ordered(sig.args[0])

    pointer=sig.args[0]

    with qtbot.waitSignal(app.signal.playAfter, 200) as sig:
        order.agent = 'Random'
    assert order.agent.is_ordered(sig.args[0])

def test_backend_order_verify_defaults_checked(app, library_factory, qtbot):
    order   = Registry.Control.OrderManager(app)
    library = library_factory()
    records = library.records
    view    = Sorted(records, app, default='title')
    pointer = view.pointer(0)
    order.enable()

    agent_artist = RandomArtist(app)
    agent_album  = RandomAlbum(app)

    order_artist = agent_artist.reorder(pointer)
    assert     agent_artist.is_ordered(order_artist)
    assert not agent_album.is_ordered(order_artist)

    order_album  = agent_album.reorder(pointer)

    assert not agent_artist.is_ordered(order_album)
    assert     agent_album.is_ordered(order_album)

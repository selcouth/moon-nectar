import inspect
import pytest

from mnectar.util.decorator import Decorator
from mnectar.util.decorator import classproperty


class DecoDefault(Decorator):
    def __init__(self, function=None, default=None):
        super().__init__(function)
        self.default = default

    def _call_wrapped(self, *arg, **kw):
        if not "default" in kw:
            kw["default"] = self.default
        return super()._call_wrapped(*arg, **kw)


def test_deco_class_access(logconfig):
    logconfig({"mnectar.util.decorator": "DEBUG3"})

    class Bar:
        @Decorator
        def foo(self, var):
            return var

        @DecoDefault
        def fubar(self, var):
            return var

    assert isinstance(Bar.foo, Decorator)
    assert isinstance(Bar.fubar, Decorator)


def test_deco_function(logconfig):
    logconfig({"mnectar.util.decorator": "DEBUG3"})

    @Decorator
    def foo(var):
        return var

    assert foo(True)


def test_deco_method(logconfig):
    logconfig({"mnectar.util.decorator": "DEBUG3"})

    class Bar:
        @Decorator
        def foo(self, var):
            return var

    assert Bar().foo(True)


def test_deco_class(logconfig):
    logconfig({"mnectar.util.decorator": "DEBUG3"})

    @Decorator
    class Bar:
        def foo(self, var):
            return var

    assert Bar().foo(True)


def test_deco_args_function(logconfig):
    logconfig({"mnectar.util.decorator": "DEBUG3"})

    @DecoDefault(default=True)
    def foo(var, default):
        return var and default

    assert foo(True)
    assert not foo(False)


def test_deco_args_method(logconfig):
    logconfig({"mnectar.util.decorator": "DEBUG3"})

    class Bar:
        @DecoDefault(default=True)
        def foo(self, var, default):
            return var and default

    assert Bar().foo(True)
    assert not Bar().foo(False)


def test_deco_args_class(logconfig):
    logconfig({"mnectar.util.decorator": "DEBUG3"})

    @DecoDefault(default=True)
    class Bar:
        def __init__(self, *, default=False):
            self.default = default

        def foo(self, var):
            return var and self.default

    assert Bar().foo(True)
    assert not Bar(default=False).foo(True)
    assert not Bar(default=True).foo(False)


def test_classproperty_get():
    class Foo:
        _bar_value = True

        @classproperty
        def bar(cls):
            return cls._bar_value

    assert Foo.bar == Foo._bar_value
    Foo._bar_value = False
    assert Foo.bar == Foo._bar_value

    obj = Foo()
    Foo._bar_value = True
    assert obj.bar == Foo._bar_value
    Foo._bar_value = False
    assert obj.bar == Foo._bar_value

    assert isinstance(inspect.getattr_static(Foo, "bar"), classproperty)

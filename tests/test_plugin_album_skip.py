import pytest
from mnectar.plugins.album_skip import AlbumSkip
from mnectar.library.view import Sorted

def test_plugin_album_skip(app, qtbot, logconfig, library_factory, setting):
    logconfig(
        {
            "mnectar.plugins.album_skip": "DEBUG3",
            "mnectar.util.signal": "DEBUG3",
        }
    )

    library = library_factory()
    records = library.records
    view    = Sorted(records, app, default='album')
    first   = view.pointer(0)
    skipper = AlbumSkip(app=app)

    with setting('playback.loop', False):
        # Test skip to next album
        with qtbot.waitSignal(app.signal.playing) as sig:
            app.signal.playing.emit(first, first.record['length'])

        with qtbot.waitSignal(app.signal.playNew) as sig:
            skipper.next_album.on_triggered()

        new_ptr = sig.args[0]

        assert new_ptr.valid
        assert new_ptr.prev.valid
        assert new_ptr.record['album'] != first.record['album']
        assert new_ptr.prev.record['album'] == first.record['album']

        second = sig.args[0]

        # Test skip to previous album: From first track in second album
        with qtbot.waitSignal(app.signal.playing) as sig:
            app.signal.playing.emit(second, second.record['length'])

        with qtbot.waitSignal(app.signal.playNew) as sig:
            skipper.prev_album.on_triggered()

        new_ptr = sig.args[0]

        assert new_ptr.valid
        assert not new_ptr.prev.valid
        assert new_ptr.record['album'] != second.record['album']
        assert new_ptr == first

        # Test skip to previous album: From second track in second album
        with qtbot.waitSignal(app.signal.playing) as sig:
            # Start from the second track
            # ... which is always available in the generated test library
            # ... this ensures the current album is not detected
            app.signal.playing.emit(second.next, second.record['length'])

        with qtbot.waitSignal(app.signal.playNew) as sig:
            skipper.prev_album.on_triggered()

        new_ptr = sig.args[0]

        assert new_ptr.valid
        assert not new_ptr.prev.valid
        assert new_ptr.record['album'] != second.record['album']
        assert new_ptr == first

        # Test stop if no previous album
        with qtbot.waitSignal(app.signal.playing) as sig:
            app.signal.playing.emit(first, first.record['length'])

        assert not first.prev.valid

        with qtbot.waitSignal(app.signal.stop) as sig:
            skipper.prev_album.on_triggered()

        # Test stop if no next album
        last = view.pointer(len(view)-1)
        with qtbot.waitSignal(app.signal.playing) as sig:
            app.signal.playing.emit(last, last.record['length'])

        assert not last.next.valid

        with qtbot.waitSignal(app.signal.stop) as sig:
            skipper.next_album.on_triggered()

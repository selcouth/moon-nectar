import pytest

from mnectar.action import Action, Actionable
from mnectar.config import Setting
from mnectar.util.signal import Signal


class ActionTester(Actionable):
    # fmt: off
    set_bool = Setting('setting.is_bool', default=False)
    set_str  = Setting('setting.is_str', default='val1')
    sig_1    = Signal()
    sig_2    = Signal()
    sig_3    = Signal()
    act_bool = Action("Menu1",         "", "SettingBool", setting=set_bool)
    act_str1 = Action("Menu1",         "", "SettingStr",  setting=set_str, args=("val1", ))
    act_str2 = Action("Menu1",         "", "SettingStr",  setting=set_str, args=("val2", ))
    act_sig1 = Action("Menu1|Submenu", "", "Signal_1",    signal=sig_1)
    act_sig2 = Action("Menu1|Submenu", "", "Signal_2",    signal=sig_2,    checkable=True)
    act_sig3 = Action("Menu1|Submenu", "", "Signal_3",    signal=sig_3,    args=(True,   True))
    act_sc   = Action("Menu1|Submenu", "", "Signal_3",    "Ctrl+O")
    sig_1_v  = False
    sig_2_v  = False
    sig_3_v  = (False, False)
    # fmt: on

    @act_bool.triggered
    def on_set_bool(self, state):
        assert self.set_bool == state

    @act_str1.triggered
    def on_set_str1(self, value):
        assert value == "val1"

    @act_str2.triggered
    def on_set_str2(self, value):
        assert value == "val2"

    @act_sig1.triggered
    def on_sig1(self):
        self.sig_1_v = True

    @act_sig2.triggered
    def on_sig2(self, state):
        self.sig_2_v = state

    @act_sig3.triggered
    def on_sig3(self, val1, val2):
        self.sig_3_v = (val1, val2)


class TestAction:
    def test_actionable(self, logconfig, app):
        logconfig({"mnectar.action": "DEBUG3"})
        obj = ActionTester(app=app)

        assert obj.act_bool._instance == obj
        assert sorted(obj.actionables.keys()) == sorted(
            _ for _ in dir(obj) if _.startswith("act_")
        )

    def test_action_bool_config(self, logconfig, app):
        logconfig({"mnectar.action": "DEBUG3"})
        obj = ActionTester(app=app)

        # Test boolean setting behavior
        assert obj.act_bool.is_checked() is False
        assert obj.act_bool.checkable
        assert obj.act_bool.get_setting() == obj.set_bool == False
        obj.set_bool = True
        assert obj.act_bool.get_setting() == obj.set_bool == True
        obj.act_bool.set_setting(False)
        assert obj.act_bool.get_setting() == obj.set_bool == False

    def test_action_str_config(self, logconfig, app):
        logconfig({"mnectar.action": "DEBUG3"})
        obj = ActionTester(app=app)

        # Test string setting (with args) behavior
        assert obj.act_str1.is_checked()
        assert not obj.act_str2.is_checked()
        obj.set_str = "val2"
        assert not obj.act_str1.is_checked()
        assert obj.act_str2.is_checked()

    def test_action_bool_trigger(self, logconfig, app):
        logconfig({"mnectar.action": "DEBUG3"})
        obj = ActionTester(app=app)

        # Test triggering (bool)
        assert obj.set_bool is False
        obj.act_bool.on_triggered(True)
        assert obj.act_bool.get_setting() == obj.set_bool == True

    def test_action_str_trigger(self, logconfig, app):
        logconfig({"mnectar.action": "DEBUG3"})
        obj = ActionTester(app=app)

        # Test triggering (str)
        assert obj.set_str == "val1"

        obj.act_str2.on_triggered()
        assert obj.set_str == "val2"
        assert not obj.act_str1.is_checked()
        assert obj.act_str2.is_checked()

        obj.act_str1.on_triggered()
        assert obj.set_str == "val1"
        assert obj.act_str1.is_checked()
        assert not obj.act_str2.is_checked()

    def test_action_signal_trigger_basic(self, logconfig, app, qtbot):
        logconfig({"mnectar.action": "DEBUG3"})
        obj = ActionTester(app=app)

        assert obj.sig_1_v is False

        with qtbot.waitSignal(obj.sig_1):
            obj.act_sig1.on_triggered()

        assert obj.sig_1_v is True

    def test_action_signal_trigger_checkable(self, logconfig, app, qtbot):
        logconfig({"mnectar.action": "DEBUG3"})
        obj = ActionTester(app=app)

        assert obj.sig_2_v is False

        with qtbot.waitSignal(obj.sig_2):
            obj.act_sig2.on_triggered(True)

        assert obj.sig_2_v is True

    def test_action_signal_trigger_args(self, logconfig, app, qtbot):
        logconfig({"mnectar.action": "DEBUG3"})
        obj = ActionTester(app=app)

        assert obj.sig_3_v == (False, False)

        with qtbot.waitSignal(obj.sig_3):
            obj.act_sig3.on_triggered()

        assert obj.sig_3_v == (True, True)

    def test_action_errors(self, logconfig, app):
        with pytest.raises(ValueError):

            class ActionTester(Actionable):
                set_bool = Setting("setting.is_bool", default=False)
                sig_1 = Signal()
                act_bool = Action("SettingBool", setting=set_bool, signal=sig_1)

        class ActionTester(Actionable):
            set_bool = Setting("setting.is_bool", default=False)
            sig_1 = Signal()
            act_bool = Action("SettingBool", setting=set_bool)

        obj = ActionTester(app=app)
        with pytest.raises(ValueError):
            obj.act_bool = False

    def test_action_shortcut(self, logconfig, app, mocker):
        logconfig({"mnectar.action": "DEBUG3"})

        obj      = ActionTester(app=app)
        callback = mocker.MagicMock()

        obj.act_sc.set_shortcut_change_callback(callback)

        assert obj.act_sc.shortcut == "Ctrl+O"

        # Test set shortcut
        obj.act_sc.shortcut = "Ctrl+L"
        assert obj.act_sc.shortcut == "Ctrl+L"
        callback.assert_called_once_with("Ctrl+L")
        callback.reset_mock()

        # Test reset shortcut
        del obj.act_sc.shortcut
        assert obj.act_sc.shortcut == "Ctrl+O"
        callback.assert_called_once_with("Ctrl+O")
        callback.reset_mock()

        # Test set no shortcut
        obj.act_sc.shortcut = ""
        assert obj.act_sc.shortcut == ""
        callback.assert_called_once_with("")
        callback.reset_mock()

        # Test reset shortcut
        obj.act_sc.shortcut = "Ctrl+O"
        assert obj.act_sc.shortcut == "Ctrl+O"
        callback.assert_called_once_with("Ctrl+O")
        callback.reset_mock()

    def test_action_shortcut_set_first(self, logconfig, app, mocker):
        logconfig({"mnectar.action": "DEBUG3"})

        obj      = ActionTester(app=app)
        callback = mocker.MagicMock()

        obj.act_sc.set_shortcut_change_callback(callback)

        # Test set shortcut before get value
        obj.act_sc.shortcut = "Ctrl+L"
        assert obj.act_sc.shortcut == "Ctrl+L"
        callback.assert_called_once_with("Ctrl+L")
        callback.reset_mock()
